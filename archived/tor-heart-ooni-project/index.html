<!doctype html>
<html>
<head>
    <title>Tor at the Heart: The OONI project | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Tor at the Heart: The OONI project | Tor Project">
    <meta property="og:description" content="During the month of December, we&#39;re highlighting other organizations and projects that rely on...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/tor-heart-ooni-project/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Tor at the Heart: The OONI project
      </h1>
    <p class="meta">by agrabeli | December 9, 2016</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog8" rel="nofollow">Donate today</a></p>

<p><i>In this post we provide an overview of <a href="https://ooni.torproject.org" rel="nofollow">OONI</a>, a project under The Tor Project.</i></p>

<p><a href="https://ooni.torproject.org" rel="nofollow"><br />
<img src="https://extra.torproject.org/blog/2016-12-08-tor-heart-ooni-project/ooni-logo-600px.png" width="200px" /><br />
</a></p>

<p><a href="https://ooni.torproject.org/" rel="nofollow">The Open Observatory of Network Interference (OONI)</a> is a free software project under The Tor Project that aims to increase transparency about internet censorship around the world. To this end, OONI has developed multiple <a href="https://ooni.torproject.org/nettest/" rel="nofollow">free software tests</a> (called <a href="https://github.com/TheTorProject/ooni-probe" rel="nofollow">ooniprobe</a>) that are designed to examine the following:</p>

<ul>
<li>Blocking of websites;</li>
<li>Blocking of Instant Messaging software such as WhatsApp and Facebook Messenger;</li>
<li>Blocking of Tor, proxies, VPNs, and sensitive domains;</li>
<li>Detection of systems responsible for censorship, surveillance and traffic manipulation.</li>
</ul>

<p>Anyone can <a href="https://ooni.torproject.org/install/" rel="nofollow">run these tests</a> to examine whether censorship is being implemented in their network. All data collected through ooniprobe is <a href="https://ooni.torproject.org/data/" rel="nofollow">published</a> and can serve as a resource for those who are interested in knowing how, when, and by whom internet censorship is being implemented. You can find OONI’s data in <a href="https://measurements.ooni.torproject.org/" rel="nofollow">JSON format</a> or via <b><a href="https://explorer.ooni.torproject.org/world/" rel="nofollow">OONI Explorer</a></b>: a global map for exploring and interacting with all the network measurement data that OONI has collected from 2012 to date.</p>

<p><a href="https://explorer.ooni.torproject.org" rel="nofollow"><br />
<img src="https://extra.torproject.org/blog/2016-12-08-tor-heart-ooni-project/ooni-explorer.png" width="400px" /><br />
</a></p>

<p>Hundreds of volunteers have run ooniprobe <a href="https://explorer.ooni.torproject.org/world/" rel="nofollow">across more than 100 countries</a> around the world, shedding light on multiple instances of internet censorship. WhatsApp, for example, was <a href="https://ooni.torproject.org/post/brazil-whatsapp-block/" rel="nofollow">found to be blocked in Brazil</a> earlier this year, while <a href="https://ooni.torproject.org/post/uganda-social-media-blocked/" rel="nofollow">Facebook and Twitter were censored</a> during Uganda’s 2016 general elections. OONI data also <a href="https://explorer.ooni.torproject.org/highlights/" rel="nofollow">shows</a> that news websites were blocked in Iran and India, amongst many other countries, and that sites supporting <a href="https://ooni.torproject.org/post/zambia-election-monitoring/" rel="nofollow">LGBTI dating also appeared to be tampered with in Zambia</a>.</p>

<p>OONI aims to equip the public around the world with data that can serve as evidence of internet censorship events. Such data not only shows whether a site or service was blocked, but more importantly, how it was blocked, when, where, and by whom. This type of information can be particularly useful to the following:</p>

<ul>
<li><strong>Lawyers:</strong> Examine the legality of the type of internet censorship implemented in your country, and use OONI’s data as evidence.</li>
<li><strong>Journalists:</strong> Improve the credibility of your stories by referencing network measurement data as evidence of censorship events.</li>
<li><strong>Researchers:</strong> Use OONI’s data to explore new questions. Researchers from the University of Cambridge and UC Berkeley, for example, were able to <a href="https://www.cl.cam.ac.uk/~sk766/publications/ndss16_tor_differential.pdf" rel="nofollow">examine</a> the differential treatment of anonymous users through the use of OONI data.</li>
<li><strong>Activists, advocates, campaigners:</strong> Inform your work based on evidence of censorship events.</li>
<li><strong>Circumvention tool projects:</strong> Inform the development of your tools and strategies based on OONI’s findings on censorship events around the world.</li>
</ul>

<p>To empower participation in censorship research, OONI has established partnerships with local non-profit organizations around the world. Some of these organizations include:</p>

<ul>
<li><a href="http://sinarproject.org/en" rel="nofollow">Sinar Project (Malaysia)<br />
</a>
</li>
<li>
<a href="https://www.codingrights.org/" rel="nofollow">Coding Rights (Brazil)</a>
</li>
<li>
<a href="http://cipit.org/" rel="nofollow">Centre for Intellectual Property &amp; Information Technology Law (Kenya)<br />
</a>
</li>
<li>
<a href="https://www.derechosdigitales.org/" rel="nofollow">Derechos Digitales (Chile)<br />
</a>
</li>
<li>
<a href="http://opennetkorea.org/" rel="nofollow">Open Net Korea (South Korea)<br />
</a>
</li>
<li>
<a href="https://www.defenddefenders.org/" rel="nofollow">The East and Horn of Africa Human Rights Defenders Project (Uganda)<br />
</a>
</li>
</ul>

<p>These partnerships involve the daily collection of network measurements from local vantage points, determining which sites and services to test per country, and analyzing measurements within social, political, and legal context. Some partners, such as Sinar Project, even organize regional workshops to teach other groups and organizations how to measure internet censorship through the use of ooniprobe.</p>

<p>The Tor Project has supported the OONI project from day 1. <a href="https://donate.torproject.org/" rel="nofollow">Donate to The Tor Project today</a> and help us continue to uncover internet censorship around the world.</p>

<p><i>Written by Maria Xynou, OONI’s Research and Partnerships Coordinator</i></p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-224585"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224585" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2016</p>
    </div>
    <a href="#comment-224585">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224585" class="permalink" rel="bookmark">Great tools that showed me</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great tools that showed me that my supposedly free liberal<br />
western europe country is using censorship.<br />
Easy to set and run, those tools are essential to understand<br />
the hows and when.</p>
<p>Best wishes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224599"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224599" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2016</p>
    </div>
    <a href="#comment-224599">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224599" class="permalink" rel="bookmark">For some reason I can not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For some reason I can not connect to the localhost:8842. Any suggestions?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224680"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224680" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  art
  </article>
    <div class="comment-header">
      <p class="comment__submitted">art said:</p>
      <p class="date-time">December 10, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224599" class="permalink" rel="bookmark">For some reason I can not</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224680">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224680" class="permalink" rel="bookmark">Which version of ooniprobe</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Which version of ooniprobe are you running (you can see this by doing ooniprobe --version)?</p>
<p>The web UI is supported in ooniprobe versions 2.0.0 and up.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224619"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224619" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2016</p>
    </div>
    <a href="#comment-224619">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224619" class="permalink" rel="bookmark">As of this writing, all</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>As of this writing, all currently manufactured, low- to mid-range and higher x86 devices, with the exception of two obsolete AMD CPUs, incorporate a security processor that is cryptographically signed, updateable, unauditable, and for which no source code or documentation has been made public. Worse, these security processors must load and continually execute this signed firmware for the system to either be brought online (AMD) or for it to remain operational (Intel). Intel calls this technology the “Management Engine” (ME), and ships a network-enabled firmware stack for the custom OS running on the dedicated ME CPU, while AMD calls it the “Platform Security Processor” (PSP), and won’t even release the x86 cores from reset until the PSP has been started from its signed firmware blob. AMD has also incorporated the PSP into its ARM CPUs, rendering them useless for libre hardware. On the low end, some unlocked ARM devices are available, but either their I/O options are severely lacking or they are not designed for general purpose computing in the first place, rendering performance even worse than expected when used in this role. RISC-V is behind even ARM in terms of maturity with no shipping general-purpose silicon or public, reproducible benchmark data at this time. ARM-based libre systems may allow libre computing to survive in some form as a retrocomputing hobby, but they will not allow libre computing to retain its dominant role in shaping the modern software world that we have all not only grown so accustomed to, but have benefited greatly from.</p></blockquote>
<p> -- <a href="https://www.crowdsupply.com/raptor-computing-systems/talos-secure-workstation/updates/a-word-on-lockdown" rel="nofollow">https://www.crowdsupply.com/raptor-computing-systems/talos-secure-works…</a></p>
<p>Until such evil ends all programmers should go on strike indefinitely.<br />
If the GCHQ/NSA insist on serving all companies with NSL gag orders that prevent people from owning their own computers, and the legal system has completely failed, then the only option left is to start bombing the spooks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-228458"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228458" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224619" class="permalink" rel="bookmark">As of this writing, all</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-228458">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228458" class="permalink" rel="bookmark">You&#039;ve convinced me there is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You've convinced me there is cause for concern.  Not panic, yet, but sufficient concern that I hope that EFF or other technical organizations will try to find persons with the requisite expertise to hunt for evidence that ME is being abused.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-225013"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225013" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-225013">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225013" class="permalink" rel="bookmark">we hope using affordably Pi</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>we hope using affordably Pi 3 install those  testing sensitive  domain  </p>
<p>equipment instant of expensive gaming Personal computer , can </p>
<p>developer per-built the software using .ova format for virtual box , </p>
<p>so they we can buy 50-100 pi3 install rest of Asia via wireless N in </p>
<p>root top and PoE (power over Ethernet) run 7/24/365 to avoid seize by local-secret </p>
<p>political </p>
<p>police&gt;_&lt;</p>
<p>Finally those sensitive domain list in public may get those Router/ </p>
<p>Switch / parents(censorship) control software to create a Database </p>
<p>and import those website format in multinational corp / Security </p>
<p>service for easy to identify that any ip/mac visit those website </p>
<p>We should advertising/marketing that software via gaming device </p>
<p>likes Xbox one / Play station pro / steam client / Dota 2 for more </p>
<p>user to know it instant of computer science PhD degree holder</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228460" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
    <a href="#comment-228460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228460" class="permalink" rel="bookmark">Tried installing ooniprobe</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tried installing ooniprobe on Debian 8 system.</p>
<p>Observed some worrisome synaptic output during installation:<br />
&gt; ...<br />
&gt; (synaptic:****): GLib-CRITICAL **: g_child_watch_add_full: assertion 'pid &gt; 0' failed<br />
&gt; ...<br />
&gt; WARNING: Passing command line arguments is deprecated<br />
&gt; ...<br />
First installation failed (tcpdump not in the dependencies but should be?)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228463"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228463" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
    <a href="#comment-228463">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228463" class="permalink" rel="bookmark">People installing ooniprobe</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>People installing ooniprobe will apparently get a file called ooni-resources.tar.gz from github.com.  (I believe the file in question is the current geoip database.) This is worrisome because github has suffered a number of dangerous security breaches, some of which apparently yielded the login credentials of thousands of developers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228468"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228468" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
    <a href="#comment-228468">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228468" class="permalink" rel="bookmark">ooniprobe appears to have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ooniprobe appears to have installed successfully on my Debian 8 system after fixing some minor problems:</p>
<p>o I am using the onion service partial mirrors of the Debian repositories and the deb.torproject.org repository.  Often I have some trouble connecting via the onion services, and to others trying this, I counsel patience, and if you have not fetched the file before synaptic times out, just try reloading again.  After installing, I find, you often have to go through the process a second time in order to obtain a few debs which were missed on the first round.  If you see a warning that you are about to install unauthenticated software, immediately cancel the download and try again later.</p>
<p>o for some reason it seems that tcpdump was not automatically installed so ooniprobe failed to run until I manually installed tcpdump.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228476"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228476" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
    <a href="#comment-228476">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228476" class="permalink" rel="bookmark">Political dissidents</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Political dissidents everywhere love CitizenLabs (from the Munk School of Journalism in Toronto, Canada) and I love the fact that OONI is working very closely with them to devise the censorship tests included in ooniprobe.</p>
<p>However, CitizenLabs appears to suffer from the same problem as Tor Project (until very recently):</p>
<p>o tiny staff, tiny budget, especially in view of their critical importance to democracy around the globe,</p>
<p>o funded by a FVEY government (Canada),</p>
<p>o (probably not by coincidence) focuses only on censorship and other abuses by governments which are not generally regarded as friendly towards the FVEY nations (US,UK,CA,AU,NZ).</p>
<p>I would like to urge OONI to remain on guard for censorship inside US and UK, because as the USG takes a sharp right turn towards something very much resembling a potentially genocidal fascism, some degree of explicit on-line censorship seems like the least nasty novelty which Americans may reasonably expect to experience in the coming years.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228477"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228477" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
    <a href="#comment-228477">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228477" class="permalink" rel="bookmark">Looking through the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Looking through the CitizenLab tests included in ooniprobe, I don't see a test for USA.  Shouldn't you have a US test, with sites such as wikileaks.org and the sites which a notorious Washington Post article designated (falsely) as "pro-Putin" [sic] "fake news" [sic] sites such as theintercept.com, techdirt.com, truthdig.com, truth-out.org*, etc.  And some sites which I think could credibly be designated as "alt-right sites", such as infowars and prisonplanet and other Alex Jones sites.</p>
<p>* The notorious leak of John Podesta's email inbox showed that he somehow got signed up to receive email alerts to truth-out.org stories.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228481"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228481" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
    <a href="#comment-228481">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228481" class="permalink" rel="bookmark">OONI is a wonderful project</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OONI is a wonderful project and I support it wholeheartedly.</p>
<p>However, you probably should have mentioned that many cybersecurity researchers have grown increasingly concerned that owing to unresolved confusion in the talks seeking to update the Wassenaar acords which govern running any kind of network probes (such as ooniprobe) could potentially result in arrest for anything from "illegal export of a cyberweapon" to "espionage", which are very serious charges in most "Western democracies".  On the bright side, anyone who experiences such a fate would probably get good legal help from ACLU, EFF, and other such organizations.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228484"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228484" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 24, 2016</p>
    </div>
    <a href="#comment-228484">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228484" class="permalink" rel="bookmark">OONI is a wonderful</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OONI is a wonderful project.</p>
<p>However, you probably should have highlighted the standard warning to read the "RISKS" page before installing ooniprobe.</p>
<p>Highlights from OONI's own "RISKS" page:</p>
<p>o ooniprobe by default will daily try to contact numerous controversial websites featuring porn, politics, and hate speech (in order to determine what kind of content is being censored),<br />
o oooniprobe tests ordinary http connections, so your true IP address will wind up in the (daily) server logs in all kinds of controversial websites,<br />
o by default the results will be sent (via hidden services) to OONI and published "anonymously",<br />
o by default your ASN (internet provider), geolocation, and the timestamp of the test will be published, which is probably enough to deanonymize you.</p>
<p>It seems to me that this implies that:</p>
<p>o if an agency with global reach such as the US FBI raids a hate speech site in FR (for example) and finds your IP in their server logs, you are very likely to wind up in the watchlists of dozens of "Western" intelligence agencies and national police agencies<br />
o if Russian intelligence finds your IP (which can sometimes be leaked in the published data although OONI tries to prevent this), consequences (even outside Russia) *may* include IRL harrassment or worse,<br />
o if Ethiopian intelligence (for examples) finds your IP, consequences are *likely* to be severe.</p>
<p>Ooniprobe is fabulous, but the world is dangerous and very rapidly becoming much more so, with such former self-proclaimed defenders of human rights as UK and USA turning sharply towards alt-right style street violence and other ugly modes of repression.</p>
<p>That said, ooniprobe tests are divided between censorship tests (the ones involving contacting controversial websites) and interferrence tests (which check for things like packet injection).  Further, it is apparently possible to configure ooniprobe to not run daily, and users can ask that their test results not be "anonymously" reported to OONI by onion service---but this might be more dangerous than letting your test results be published:</p>
<p>Currently, if an US intelligence agency detects you running ooniprobe, it might actually help your defense if a US LEA arrests you because your IP turned up in the server logs of some porn site.  But this judgment is subject to rapid unpredictable flux as the incoming US administration starts to decide what kinds of specific policies to adopt.</p>
<p>I stress that OONI documents clearly state that to their knowledge, no resident of a "Western democracy" has yet suffered arrest as the result of running ooniprobe.  But as more and more US LEAs rush to arrest people using increasingly flawed electronic evidence, confusion owing to the chaotic sharing of flawed data is rapidly increasing, and I fear it is virtually certain that running ooniprobe will turn out to be too dangerous for anyone who does not have a good lawyer on retainer.</p>
<p>I say this not to criticize,but only just to discuss ways to improve our game.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-228992"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-228992" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 27, 2016</p>
    </div>
    <a href="#comment-228992">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-228992" class="permalink" rel="bookmark">But the US and the EU ar</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>But the US and the EU ar blocking pirate websites. Why are not they red?</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
