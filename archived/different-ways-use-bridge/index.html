<!doctype html>
<html>
<head>
    <title>Different Ways to Use a Bridge | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Different Ways to Use a Bridge | Tor Project">
    <meta property="og:description" content="Different Ways to Use a Bridge When some adversary prevents users from reaching the Tor network,...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/different-ways-use-bridge/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Different Ways to Use a Bridge
      </h1>
    <p class="meta">by Sebastian | November 29, 2011</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><strong>Different Ways to Use a Bridge</strong></p>

<p>When some adversary prevents users from reaching the Tor network, our most popular answer is using bridge relays (or bridges for short). Those are hidden relays, not listed along with all the other relays in the networkstatus documents.  Currently, we have about 600 of them, and censors are having different luck learning and blocking them — see <a href="https://blog.torproject.org/blog/research-problems-ten-ways-discover-tor-bridges" rel="nofollow">the 10 ways to discover Tor bridges blog post</a> for more on how discovery approaches may work. China appears to be the only place able to successfully block most bridges consistently, whereas other places occasionally manage to block Tor's handshake and as a byproduct block all bridges too.</p>

<p>Bridge users can be broadly grouped in three camps:</p>

<ul>
<li>Tor is blocked, and some way — any way — to reach the network has to be found. The adversary is not very dangerous, but very annoying.</li>
<li>Tor may or may not be blocked, but the user is trying to hide the fact they're hiding Tor. The adversary may be extremely dangerous.</li>
<li>Other bridge users: Testing whether the bridge works (automated or manual), probing, people using bridges without their knowledge because they came pre-configured in their bundle.</li>
</ul>

<p>Here we examine the first two use cases more closely. Specifically, we want to look at properties of a bridge that must exist for it to be useful to a user.</p>

<p><strong>Bridges — building blocks</strong></p>

<p>First off, it is helpful to understand some basics about bridges and how they are used by normal users.</p>

<p>Bridges are very similar to ordinary relays, in that they are operated by volunteers who made the decision to help people reach the Tor network. The difference to a normal relay is where the information about the bridge is published to — bridges can choose to either publish to the Bridge Authority (a special relay collecting all bridge addresses that it receives), or to not publish their information anywhere. The former are called public bridges, the latter private bridges.</p>

<p>We don't have any information about the number of private bridges, but since the Bridge Authority collects data about the public bridges, we do know that bridges are used in the real world. See <a href="https://metrics.torproject.org/users.html#bridge-users" rel="nofollow">the bridge users</a> and <a href="https://metrics.torproject.org/network.html#networksize" rel="nofollow">networksize</a> graphs for some examples. Not having data about private bridges or their users means some of the analysis below is based on discussions with users of private bridges and our best estimates, and it can't be backed up by statistical data.</p>

<p>The reason we're using a Bridge Authority and collecting information about bridges is that we want to give out bridges to people who aren't in a position to learn about a private bridge themselves.</p>

<p>"Learning about a bridge" generally means learning about the bridge's IP address and port, so that a connection can be made. Optionally, the bridge identity fingerprint is included, too — this helps the client to verify that it is actually talking to the bridge, and not someone that is intercepting the network communication. For a private bridge, the operator has to pass on that information; public bridges wrap up some information about themselves in what is called their bridge descriptor and send that to the bridge authority. The bridge descriptor includes some statistical information, like aggregated user counts and countries of origin of traffic. Our analysis here focuses solely on the data provided by public bridges.</p>

<p>Once a user has learned about some bridges, she configures her Tor client to use them, typically by entering them into the appropriate field in Vidalia. Alternatively, she might use a different controller or put the data into tor's configuration file directly.</p>

<p><strong>Learning from bridge descriptor fetches</strong></p>

<p>We've been collecting bridge descriptor fetch statistics on the bridge authority, and are using this data to pose some questions and propose some changes. The statistics collected are how many bridge descriptors were served in total, and how many unique descriptors were served, as well as the 0, 25, 50, 75 and 100 percentiles of fetches per descriptor. Every 24 hours, the current statistics are written to disk and the counters reset. The current statistics are attached to this post, for closer inspection. We've also prepared two graphs to easily see the data at a glance:</p>

<p><img src="https://blog.torproject.org/static/images/blog/inline-images/bridge-total-downloads.png" alt="Total bridge downloads" width="100%" /></p>

<p><img src="https://blog.torproject.org/static/images/blog/inline-images/bridge-downloads-per-desc.png" alt="Bridge downloads per descriptor" width="100%" /></p>

<p>The first thing to note is that there aren't very many bridge descriptor fetches at all, which isn't a big surprise: The current Tor bundles don't fetch them when they're used in the typical way, that is by adding some bridges via Vidalia's interface after bridges were discovered via one of our bridge distribution channels. Over the past month, there have been between 3900 and 6600 fetches per day, with a median of 8 fetches per bridge. The most fetched descriptor is fetched up to 350 times per day, indicating that it does indeed belong to a bridge that was given out with a fingerprint and being used by Tor clients. We have gotten some reports that a bundle circulated with pre-configured bridges, and this could account for the many fetches.</p>

<p>Secondly, most bridge descriptors are not even fetched from the authority. This is a clear indication that we can improve our odds of updating bridge clients with current bridge info if we can get them to request the information better.</p>

<p><strong>Improving Tor's behaviour for the two user groups</strong></p>

<p>The first group ("Tor is blocked, and some way to reach the network has to be found") is mostly concerned about circumvention, without necessarily hiding that they're using Tor from someone. Typically, access to the Internet is filtered, but circumventing a filter isn't too risky and people are more concerned with access than hiding their tracks from a data-collecting adversary. Speed, bootstrapping performance, and little intervention/maintenance of a setup are the biggest goals.</p>

<p>Adding auto-discovery mechanisms for bridges that changed their IP address will help this group gain a lot more robustness when it comes to maintaining connectivity against an adversary that blocks public relays, but isn't very quick in blocking all bridges. As far as we know, this is currently true for the majority of our bridge userbase.</p>

<p>For the second group ("Tor may or may not be blocked, but the user is trying to hide the fact they're hiding Tor"), precise control over Tor's actions is much more important than constant connectivity, and private bridges might be utilized to that end as well. A user in this group wants to keep the bridges he's using secret, and puts up with frequent updates to the configuration for the added safety of only connecting to a pre-specified IP address:port combination. We can't do very much for a user belonging to this group with regard to bridges, but he will very much benefit from improvements made to our general fingerprintability resistance. Also options like the <a href="https://trac.torproject.org/projects/tor/ticket/3644" rel="nofollow">DisableNetwork</a> option (prevent touching the network in any kind of way until this option is changed) that was recently introduced to Tor help him.</p>

<p>Another interesting point here is that we can indirectly improve the behaviour for the first group by not making it too easy to learn about bridges, because censors can use the same data to more effectively block them. This means that we shouldn't, for example, start giving out significantly more bridges to a single user.</p>

<p>We've written a <a href="https://lists.torproject.org/pipermail/tor-dev/2011-November/003097.html" rel="nofollow">proposal</a> to implement some changes in Tor, to better facilitate the needs of the first group of bridge users.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-13026"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13026" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2011</p>
    </div>
    <a href="#comment-13026">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13026" class="permalink" rel="bookmark">We make a Tor bundle with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We make a Tor bundle with pre-configured bridges. We are not 100% sure if all the bridges are up and running.</p>
<p>Our problem, being English speakers, is communicating the need for bridges and how to use them to the Chinese user base we have.</p>
<p>Discovering bridges for the Chinese seems to be the biggest problem. We bundle a p2p client to help them talk to us so that in turn we can help them. China can't just be blocking public Tor bridges since we have tried several private ones, with Tor reaching 90% on connecting before giving up. As mentioned in the article, maybe they are blocking negotiation traffic of Tor and therefore Tor in its entirety.</p>
<p>We think the new features of 0.2.3.9-alpha will help. We'd like to investigate obfuscation techniques for Tor traffic. The new "transport" proxy feature may help, but is configuration of this difficult, is there any help in this regard ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
