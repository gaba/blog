<!doctype html>
<html>
<head>
    <title>Defend Dissent with Tor | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Defend Dissent with Tor | Tor Project">
    <meta property="og:description" content="Guest post by Glencora Borradaile After 4 years of giving digital security trainings to...">
    <meta property="og:image" content="https://blog.torproject.org/book-defend-dissent-with-tor/lead.jpg">
    <meta property="og:url" content="https://blog.torproject.org/book-defend-dissent-with-tor/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Defend Dissent with Tor
      </h1>
    <p class="meta">by ggus | April 27, 2021</p>
    <picture>
      
      <img class="lead" src="lead.jpg">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p><em>Guest post by Glencora Borradaile</em></p>
<p>After 4 years of giving digital security trainings to activists and teaching a course called "Communications Security and Social Movements", I've compiled all my materials into an open, digital book - <a href="https://open.oregonstate.education/defenddissent/">Defend Dissent: Digital Suppression and Cryptographic Defense of Social Movements</a> hosted by Oregon State University where I am an Associate Professor. The book is intended for an introductory, non-major college audience, and I hope it will find use outside the university setting.</p>
<p>Defend Dissent has three parts:</p>
<ul>
<li>Part 1 covers the basics of cryptography: basic encryption, how keys are exchanged, how passwords protect accounts and how encryption can help provide anonymity.  When I give digital security trainings, I don't spend a lot of time here, but I still want people to know (for example) what end-to-end encryption is and why we want it.</li>
<li>Part 2 gives brief context for how surveillance is used to suppress social movements, with a US focus.</li>
<li>Part 3 contains what you might consider more classic material for digital security training, focusing on the different places your data might be vulnerable and the tactics you can use to defend your data.</li>
</ul>
<p>Each chapter ends with a story that brings social context to the material in that chapter (even in Part 1!) - from surveillance used against contemporary US protests to the African National Congress' use of partially manual encryption in fighting apartheid in South Africa in the 80s.</p>
<p>It should be no surprise that Tor is a star of Defend Dissent, ending out Parts 1 and 3. The anonymity that the Tor technology enables turns the internet into what it should be: a place to communicate without everyone knowing your business. As a professor, I love teaching Tor. It is a delightful combination of encryption, key exchange, probability and threat modeling.</p>
<p>In Defend Dissent, I aim to make Tor easy to understand, and use a three-step example to explain Tor to audiences who may have never used it before: There are just three steps to understanding how Tor works:</p>
<p>1. <strong>Encryption</strong> allows you to keep the content of your communications private from anyone who doesn't have the key. But it doesn't protect your identity or an eavesdropper from knowing who you are communicating with and when.</p>
<p><img alt="Encryption keeps the content of your communications private" height="103" src="/static/images/blog/inline-images/E2EE.png" width="543" /></p>
<p>2. Assata can send Bobby an encrypted message even if they haven't met ahead of time to agree on a key for encryption. This concept can be used to allow Assata and Bobby to agree on a single <strong>encryption key</strong>. (Put an encryption key in the box.)</p>
<p><img alt="Exchanging a secure message without sharing a key" height="305" src="/static/images/blog/inline-images/lockbox-a.gif" width="543" /></p>
<p>3. When Assata accesses Tor, the Tor Browser picks three <strong>randomly chosen nodes</strong> (her entry, relay and exit nodes) from amongst thousands in the Tor network. Assata's Tor Browser agrees on a key with the entry node, then agrees on a key with the relay node by communicating with the relay node <strong>through</strong> the entry node, and so on. Assata's Tor Browser encrypts the message with the exit key, then with the relay key and then with the entry key and sends the message along. The entry node removes one layer of encryption and so on. (Like removing the layers of an onion ...) This way, the relay doesn't know who Assata is - just that it is relaying a message through the Tor network.</p>
<p><img src="https://media.torproject.org/video/anonymous-browsing-tor-a.gif" /></p>
<p>I'm excited to share this accessible resource and to teach the world more about Tor, encryption, and secure communication. Even if you're a technical expert, Defend Dissent may help you talk to others in your life about how to use Tor and why these kinds of tools are so vital to social movements, change, and dissent.</p>
<p>For more details on how Tor works you can read the four chapters of Defend Dissent that lead to <a href="https://open.oregonstate.education/defenddissent/chapter/anonymous-routing/">Anonymous Routing</a>: <a href="https://open.oregonstate.education/defenddissent/chapter/what-is-encryption/">What is Encryption?</a>, <a href="https://open.oregonstate.education/defenddissent/chapter/modern-cryptography/">Modern Cryptography</a>, <a href="https://open.oregonstate.education/defenddissent/chapter/exchanging-keys-for-encryption/">Exchanging Keys for Encryption</a>, and <a href="https://open.oregonstate.education/defenddissent/chapter/metadata/">Metadata</a>.<br />
Or discover other topics in <a href="https://open.oregonstate.education/defenddissent/wp-admin/admin-ajax.php?action=h5p_embed&amp;id=2">defending social movements with cryptography</a>.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li><li>
        <a href="../category/community">
          community
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Comments are closed.</p>
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
