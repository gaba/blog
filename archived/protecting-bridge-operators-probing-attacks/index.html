<!doctype html>
<html>
<head>
    <title>Protecting bridge operators from probing attacks | The Tor Project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="monetization" content="$ilp.uphold.com/pYfXb92JBQN4">
    <link rel="stylesheet" href="../static/css/style.css?h=5fc6c25a">
    <link rel="stylesheet" href="../static/fonts/fontawesome/css/all.min.css?h=9d272f6a">
    <link rel="stylesheet" href="../static/pygments.css">
    <link rel="icon" type="image/x-icon" href="../static/images/favicon/favicon.ico">
    <link rel="icon" type="image/png" href="../static/images/favicon/favicon.png">
    <meta property="og:title" content="Protecting bridge operators from probing attacks | Tor Project">
    <meta property="og:description" content="Although Tor was originally designed to enhance privacy, the network is increasingly being used...">
    <meta property="og:image" content="https://blog.torproject.org/static/images/lead.png">
    <meta property="og:url" content="https://blog.torproject.org/protecting-bridge-operators-probing-attacks/">
    <meta name="twitter:card" content="summary_large_image">
</head>
<body>
  <header>
    <nav>
      <a class="navbar-brand" href="../"><img alt="Tor Blog" src="../static/images/logo.png" /></a>
      <ul class="navbar-nav">
          <li><a href="https://www.torproject.org/about/history/">About</a></li>
          <li><a href="https://support.torproject.org/">Support</a></li>
          <li><a href="https://community.torproject.org/">Community</a></li>
          <li><a href="https://forum.torproject.org/">Forum</a></li>
          <li><a href="https://donate.torproject.org/">Donate</a></li></ul>
    </nav>
  </header>
  <section class="content">
    <main>
  <article class="blog-post">
      <h1 class="title">
        Protecting bridge operators from probing attacks
      </h1>
    <p class="meta">by sjmurdoch | March 15, 2012</p>
    <picture>
      <source media="(min-width:415px)" srcset="../static/images/lead.webp" type="image/webp">
<source srcset="../static/images/lead_small.webp" type="image/webp">

      <img class="lead" src="../static/images/lead.png">
    </picture>
    <div class="body">
      <link rel="stylesheet" href="../static/css/legacy.css?h=21ddbb2d">
      <p>Although Tor was originally designed to enhance privacy, the network is increasingly being used to provide access to websites from countries where Internet connectivity is filtered. To better support this goal, Tor introduced the feature of <a href="https://www.torproject.org/docs/bridges" rel="nofollow">bridge nodes</a> – Tor nodes which route traffic but are not published in the list of available Tor nodes. Consequently bridges are commonly not blocked in countries where access to the public Tor nodes are (with the exception of China and Iran).</p>

<p>For bridge nodes to work well, we need lots of them. This is both to make it hard to block them all, and also to provide enough bandwidth capacity for all their users. To help achieve this goal, the Tor Browser Bundle now makes it as easy as clicking a button to turn a normal Tor client into a bridge. The upcoming version of Tor will even set up port forwarding on home routers (through UPnP and NAT-PMP) to make it easier still.</p>

<p>There are, however, potential risks to Tor users if they turn their client into a bridge. These were summarized in a paper by Jon McLachlan and Nicholas Hopper: <a href="http://www-users.cs.umn.edu/~hopper/surf_and_serve.pdf" rel="nofollow">"On the risks of serving whenever you surf"</a>. I discussed these risks in a <a href="https://blog.torproject.org/blog/risks-serving-whenever-you-surf" rel="nofollow">previous blog post</a>, along with ways to mitigate them. In this blog post I'll cover some initiatives the Tor project is working on (or considering), which could further reduce the risks.</p>

<p>The attack than McLachlan and Hopper propose involves finding a list of bridges, and then probing them over time. If the bridge responds at all, then we know that the bridge operator could be surfing the web though Tor. So, if the attacker suspects that a blogger is posting through Tor and the blogger's Tor client is also a bridge, then only the bridges which are running at the time a blog post was written could belong to the blogger. There are likely to be quite a few such bridges, but if the attack is repeated, the set of potential bridge IP addresses will be narrowed down.</p>

<p>What is more, the paper also proposes measuring how quickly each bridge responds, which might tell the attacker something about how heavily the bridge operator is using their Tor client. If the attacker probes the rest of the Tor network, and sees a node with performance patterns similar to the bridge being probed, then it is likely that there is a connection going through that node and that bridge. In some circumstances this attack could be used to track connections all the way through the Tor network, and back to a client who is also acting as a bridge.</p>

<p>One way of reducing the risk of these attacks succeeding is to run the bridge on a device which is always on. In this case, just because a bridge is running doesn't mean the operator is using Tor. Therefore less information is leaked to a potential attacker. As laptops become more prevalent, keeping a bridge on 24/7 can be inconvenient so the Tor Project is working on two bits of hardware (known as <a href="https://trac.torproject.org/projects/tor/wiki/doc/Torouter" rel="nofollow">Torouters</a>) which can be left running a bridge even if the user's computer is off. If someone probes the bridge now, they can't learn much about whether the operator is using Tor as a client, and so it leaks less information about what the user is doing.</p>

<p>Having an always-on bridge helps resist profiling based on when a client is on. However, evaluating the risk of profiling based on bridge performance is more complex. The reason this attack works is that when a node is congested, increasing traffic on one connection (say, due to the bridge operator using Tor) decreases the traffic on another (say, the attacker probing the bridge). If you run a Tor client on your PC, and a Tor bridge on a separate device, this reduces the opportunity for congestion on one leading to congestion on the other. However, there is still the possibility for congestion on the network connection which the bridge and device share, and maybe you do want to use the bridge device as a client too. So the Torouter helps, but doesn't fix the problem.</p>

<p>To fully decouple bridges from clients, they need to run on different hardware <strong>and networks</strong>. Here the <a href="https://cloud.torproject.org/" rel="nofollow">Tor Cloud</a> project is ideal – bridges run on Amazon's (or another cloud provider's) infrastructure so congestion on the bridge can't affect the Tor client running on your PC, or it's network.</p>

<p>But maybe you do want to use your bridge as a client for whatever reason. Recall that the first step of the attack proposed by McLachlan and Hopper is to find the bridges' IP addresses. This is becoming increasingly difficult as more bridges are being set up. Also, there is work in progress to make it harder to scan networks for bridges. Originally this was intended to make it harder to find and block bridges, but it applies equally well to preventing probing. These schemes (e.g. <a href="http://www.cypherpunks.ca/~iang/pubs/bridgespa-wpes.pdf" rel="nofollow">BridgeSPA</a> by Smits et. al.) mean that just because you have a bridge's IP address, it doesn't mean that you can route traffic through it (to measure performance) or even check if the bridge is running. To do either, you need to have a secret which is distributed only to authorized users of the bridge.</p>

<p>There is still more work to be done in protecting bridge operators, but the projects discussed above (and in the <a href="https://blog.torproject.org/blog/risks-serving-whenever-you-surf" rel="nofollow">previous blog post</a>) certainly improve the situation. Work continues both in academia and at the Tor Project to better understand the problem and develop further fixes.</p>

    </div>
  <div class="categories">
    <ul><li>
        <a href="../category/circumvention">
          circumvention
        </a>
      </li><li>
        <a href="../category/research">
          research
        </a>
      </li></ul>
  </div>
  <div class="comments">
      <h2>Comments</h2>
      <p>Please note that the comment area below has been archived.</p>
      <a id="comment-14606"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14606" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 15, 2012</p>
    </div>
    <a href="#comment-14606">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14606" class="permalink" rel="bookmark">How do they block bridges in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do they block bridges in China?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14609"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14609" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2012</p>
    </div>
    <a href="#comment-14609">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14609" class="permalink" rel="bookmark">Please release a new version</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please release a new version of TBB, because.....</p>
<p><a href="http://www.securityfocus.com/bid/52458" rel="nofollow">http://www.securityfocus.com/bid/52458</a><br />
<a href="http://www.securityfocus.com/bid/52460" rel="nofollow">http://www.securityfocus.com/bid/52460</a><br />
<a href="http://www.securityfocus.com/bid/52461" rel="nofollow">http://www.securityfocus.com/bid/52461</a><br />
<a href="http://www.securityfocus.com/bid/52464" rel="nofollow">http://www.securityfocus.com/bid/52464</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
  </div>
  </article>

</main>
    <aside class="sidebar">
<!-- ##SIDEBAR## -->
</aside>
  </section>
  <footer><div class="row download">
    <div class="col circles"></div>
    <div class="col link">
        <h3>Download Tor Browser</h3>
        <p>Download Tor Browser to experience real private browsing without tracking, surveillance, or censorship.</p>
        <a class="btn" href="https://www.torproject.org/download/">Download Tor Browser <i class="fas fa-arrow-down-png-purple"></i></a>
    </div>
</div>
<div class="row social">
    <div class="col newsletter">
        <h3>Subscribe to our Newsletter</h3>
        <p>Get monthly updates and opportunities from the Tor Project:</p>
        <p class="w"><a class="btn btn-dark" role="button" href="https://newsletter.torproject.org/">Sign up</a></p>
    </div>
    <div class="col links">
        <div class="row">
            <h4><a target="_blank" href="https://www.facebook.com/TorProject/"><i class="fab fa-facebook"></i></a></h4>
            <h4><a target="_blank" href="https://mastodon.social/@torproject" rel="me"><i class="fab fa-mastodon"></i></a></h4>
            <h4><a target="_blank" href="https://twitter.com/torproject"><i class="fab fa-twitter"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://www.instagram.com/torproject"><i class="fab fa-instagram"></i></a></h4>
            <h4><a target="_blank" href="https://www.linkedin.com/company/tor-project"><i class="fab fa-linkedin"></i></a></h4>
            <h4><a target="_blank" href="https://github.com/torproject"><i class="fab fa-github"></i></a></h4>
        </div>
        <div class="row">
            <h4><a target="_blank" href="https://t.me/torproject"><i class="fab fa-telegram"></i></a></h4>
        </div>
    </div>
</div>
<div class="row notice">
    <p>Trademark, copyright notices, and rules for use by third parties can be found in our <a href="https://www.torproject.org/about/trademark/">FAQ</a>.</p>
</div></footer>
</body>
</html>
