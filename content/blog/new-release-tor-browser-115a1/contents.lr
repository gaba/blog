title: New Alpha Release: Tor Browser 11.5a1 (Windows, macOS, Linux)
---
pub_date: 2021-12-14
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 11.5a1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a1 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a1/).

This is the first alpha version in the 11.5 series. This version updates Firefox to 91.4.0esr, which includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2021-53/).

We are also fixing some of the known issues that were introduced with Tor Browser 11.0. In particular, this release should fix the crashes a lot of our Windows users have been seeing.

Note: A new PGP subkey was used for signing this release. You may need to refresh your keychain from keys.openpgp.org to get the [updated key](https://keys.openpgp.org/vks/v1/by-fingerprint/EF6E286DDA85EA2A4BA7DE684E2C6E8793298290).

## Full changelog

The full changelog since [Tor Browser 11.0a10](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
  - Update Firefox to 91.4.0esr
  - Tor Launcher 0.2.32
  - [Bug 40059](https://gitlab.torproject.org/tpo/applications/torbutton/-/issues/40059): YEC activist sign empty in about:tor on RTL locales
  - [Bug 40386](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40386): Add new default obfs4 bridge "deusexmachina"
  - [Bug 40393](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40393): Point to a forked version of pion/dtls with fingerprinting fix
  - [Bug 40394](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40394): Bump version of Snowflake to 221f1c41
  - [Bug 40438](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40438): Add Blockchair as a search engine
  - [Bug 40646](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40646): Revert tor-browser#40475 and inherit upstream fix
  - [Bug 40680](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40680): Prepare update to localized assets for YEC
  - [Bug 40682](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40682): Disable network.proxy.allow_bypass
  - [Bug 40684](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40684): Misc UI bug fixes in 11.0a10
  - [Bug 40686](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40686): Update Onboarding link for 11.0
  - [Bug 40689](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40689): Change Blockchair Search provider's HTTP method
  - [Bug 40690](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40690): Browser chrome breaks when private browsing mode is turned off
  - [Bug 40691](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40691): Make quickstart checkbox gray when "off" on about:torconnect
  - [Bug 40698](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40698): Addon menus missing content in TB11
  - [Bug 40700](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40700): Switch Firefox recommendations off by default
  - [Bug 40705](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40705): "visit our website" link on about:tbupdate pointing to different locations
  - [Bug 40706](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40706): Fix issue in HTTPS-Everywhere WASM
  - [Bug 40714](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40714): Next button closes "How do circuits work?" onboarding tour
  - [Bug 40718](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40718): Application Menu items should be sentence case
  - [Bug 40721](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40721): Tabs crashing on certain pages in TB11 on Win 10
  - [Bug 40725](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40725): about:torconnect missing identity block content on TB11
  - Translations update
- Linux
  - [Bug 40318](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40318): Remove check for DISPLAY env var in start-tor-browser
  - [Bug 40387](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40387): Remove some fonts on Linux
