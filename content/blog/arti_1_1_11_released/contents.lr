title: Arti 1.1.11 is released: More onion progress
---
author: nickm
---
pub_date: 2023-12-04
---
categories: announcements
---
summary:

Arti 1.1.11 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.1.11.

Arti 1.1.11 continues work on support for onion services in Arti,
and we're even closer than we were last month.
We think that the odds are good that our next release
will be the one in which they're finally ready for testing
by others.
You can find a list of what we still need to do
[on the bugtracker].

This release also makes some changes affecting compatibility.
Notably: we now require Rust 1.70 or later
(in accordance with our [MSRV policy]).
Also, we have upgraded the versions
of the [dalek-cryptography] crates.
Since these crates' types appear in our public APIs,
this represents a breaking change in `arti-client`
and most lower-level crates.

For full details on what we've done, and for information about
many smaller and less visible changes as well,
please see the [CHANGELOG].

As a heads-up: the Arti team is going to be on break
around the start of the new year,
so our start-of-January release will likely be
a week or so after the actual start of the month.

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Alexander Færøy, Andrew, Jim Newsome, rdbo, Saksham Mittal, and
Trinity Pointard.

Also, our deep thanks to [Zcash Community Grants] and our [other sponsors]
for funding the development of Arti!

[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-1111-4-december-2023
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[other sponsors]: https://www.torproject.org/about/sponsors/
[on the bugtracker]: https://gitlab.torproject.org/tpo/core/arti/-/issues/?label_name%5B%5D=Onion%20Services%3A%20Basic%20Service
[MSRV policy]: https://gitlab.torproject.org/tpo/core/arti/#minimum-supported-rust-version
[dalek-cryptography]: https://github.com/dalek-cryptography/


