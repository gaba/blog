title: Arti 0.1.0 is released: Your somewhat-stable API is here!
---
author: nickm
---
pub_date: 2022-03-01
---
categories: announcements
---
summary:
Arti 0.1.0 is released, and available for download.
---
body:


Arti is our ongoing project to create a working embeddable Tor client in Rust. It’s nowhere near ready to replace the main Tor implementation in C, but we believe that it’s the future.

Today, we've reached our 0.1.0 milestone: this means that we now
consider Arti's high-level APIs to be "mostly stable", and ready for
experimental embedding in other projects.  (We don't promise no API
breakage, but we don't break our high-level APIs without a good
reason.)  The 1.0.0 milestone, scheduled for September, will represent
an even stronger API commitment.

# What's new in 0.1.0?

From a developer's point of view the primary new features are our [builder API] for
constructing `TorClient` instances, and the ability to create
[unbootstrapped] and [bootstrap-on-demand] `TorClient` instances.  These
features make it easier to create a "lazy" TorClient that bootstraps
itself in the background the first time it's used: we've provided some
[example code] to show you how.

We've also replaced our top-level [`Error` API].  Previously, it exposed
a huge mass of APIs from lower-level crates, and encouraged dependencies
on unstable lower-level error types.  Now the error details are hidden,
and there is instead an explicitly supported [`ErrorKind`] type that
programs can use to see what type of error occurred.  (You can still
access the low-level information by enabling an optional trait, if you
really want to.)

There are also a **bunch** of smaller features, bugfixes, and
infrastructure improvements; see the [changelog] for a more complete
list.

[builder api]: https://docs.rs/arti-client/0.1.0/arti_client/struct.TorClientBuilder.html
[unbootstrapped]: https://docs.rs/arti-client/0.1.0/arti_client/#creating-a-client-and-connecting-later
[bootstrap-on-demand]: https://docs.rs/arti-client/0.1.0/arti_client/#creating-a-client-and-connecting-later
[example code]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/crates/arti-client/examples/lazy-init.rs
[`Error` API]: https://docs.rs/arti-client/0.1.0/arti_client/struct.Error.html
[`ErrorKind`]: https://docs.rs/arti-client/0.1.0/arti_client/enum.ErrorKind.html
[changelog]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md


# And what's next?

Between now and our 1.0.0 milestone in September, we're aiming to make
Arti a production-quality Tor client for direct internet access.  (Onion
services aren't funded yet, but we hope to change that soon.)

To do so, we need to bring Arti up to par with the C tor implementation
in terms of its [network performance], [CPU usage], [resiliency], and
[security features].  You can follow our progress on our [1.0.0
milestone].

We still plan to continue regular releases between now and then.

[network performance]: https://gitlab.torproject.org/tpo/core/arti/-/issues/88
[CPU usage]: https://gitlab.torproject.org/tpo/core/arti/-/issues/87
[resiliency]: https://gitlab.torproject.org/tpo/core/arti/-/issues/329
[security features]: https://gitlab.torproject.org/tpo/core/arti/-/issues/331
[1.0.0 milestone]: https://gitlab.torproject.org/tpo/core/arti/-/issues?scope=all&state=opened&milestone_title=Arti%201.0.0%3A%20Ready%20for%20production%20use


# Here's how to try it out

We rely on users and volunteers to find problems in our software and
suggest directions for its improvement.  Although Arti isn't yet ready
for production use, you can test it as a SOCKS proxy (if you're willing
to compile from source) and as an embeddable library (if you don't mind
a little API instability).

Assuming you've installed Arti (with `cargo install arti`, or directly
from a cloned repository), you can use it to start a simple SOCKS proxy
for making connections via Tor with:

```
$ arti proxy -p 9150
```

and use it more or less as you would use the C Tor implementation!

(It doesn't support onion services yet.  If compilation doesn't work, make sure you have development files for libsqlite installed on your platform.)

If you want to build a program with Arti, you probably want to start with the [`arti-client`](https://docs.rs/arti-client/latest/arti_client/index.html) crate.  Be sure to check out the [examples](https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti-client/examples) too.

For more information, check out the [README](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md) file. (For now, it assumes that you're comfortable building Rust programs from the command line).  Our [CONTRIBUTING](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CONTRIBUTING.md) file has more information on installing development tools, and on using Arti inside of Tor Browser. (If you want to try that, please be aware that Arti doesn't support onion services yet.)

When you find bugs, please report them [on our bugtracker](https://gitlab.torproject.org/tpo/core/arti/).  You can [request an account](https://anonticket.torproject.org/) or [report a bug anonymously](https://anonticket.torproject.org/).

And if this documentation doesn't make sense, please ask questions! The questions you ask today might help improve the documentation tomorrow.

# Call for feedback

Our priority for the coming months is to make Arti a production-quality Tor client, for the purposes of direct connections to the internet.  (Onion services will come later.)  We know some of the steps we'll need to take to get there, but not all of them: we need to know what's missing _for your use-cases_.

Whether you're a user or a developer, please give Arti a try, and let us know what you think.  The sooner we learn what you need, the better our chances of getting it into an early milestone.

# Acknowledgments

Thanks to everybody who has contributed to this release, including
Daniel Schischkin, Dimitris Apostolou, Michael Prantl, tharvik, Trinity
Pointard, and Yuan Lyu!


And thanks, of course, to [Zcash Open Major Grants (ZOMG)](https://grants.zfnd.org/proposals/215972995-arti-a-pure-rust-tor-implementation-for-zcash-and-beyond) for funding this project!
