title: Responding to Tor censorship in Russia
---
pub_date: 2021-12-07
---
author: ggus
---
summary: Since December 1st, some Internet providers in Russia have started to block access to Tor. We need your help NOW to keep Russians connected to Tor!
---
categories:

community
circumvention
relays
---
body:

**Update:** Right after we published this article, the Russian government has officially [blocked our main website in Russia](https://gitlab.torproject.org/tpo/community/support/-/issues/40050#note_2765035). Users can circumvent this block by visiting [our website mirror](https://tor.eff.org).

Since December 1st, [some Internet providers in Russia have started to block access to Tor](https://meduza.io/news/2021/12/03/zhiteli-rossii-pozhalovalis-na-blokirovku-tor). Today, we've learned that the Federal Service for Supervision of Communications, Information Technology and Mass Media (Roskomnadzor), a Russian government bureaucratic entity, is [threatening to censor](https://gitlab.torproject.org/tpo/community/support/-/issues/40050#note_2764565) our main website (`torproject.org`). Russia is the country with the [second largest number of Tor users](https://metrics.torproject.org/userstats-relay-table.html), with more than 300,000 daily users or 15% of all Tor users. As it seems this situation could quickly escalate to a country-wide Tor block, it's **urgent that we respond to this censorship!** We need your help **NOW** to keep Russians connected to Tor!

## Run a Tor bridge

Last month we launched the campaign [Help Censored Users, Run a Tor Bridge](https://forum.torproject.net/t/help-censored-users-run-a-tor-bridge/704) to motivate more volunteers to spin up more bridges. The campaign has been a great success, and we've already achieved our goal of 200 new obfs4 bridges. Today, we have more than 400 new bridges.

But now, if the censorship pattern that we're analyzing in some Russian internet providers is to be deployed country-wide, **we will need many more bridges to keep Russians online**. Thanks to researchers, we've learned that the default bridges available in Tor Browser aren't working in some places in Russia - this includes Snowflake bridges and obfs4 bridges obtained dynamically using [Moat](https://support.torproject.org/glossary/moat/). Russian users need to [follow our guide to use bridges that are not blocked](https://forum.torproject.net/t/tor-blocked-in-russia-how-to-circumvent-censorship/982).

**We are calling on everyone to spin up a Tor bridge!** If you've ever considered running a bridge, now is an excellent time to get started, as your help is urgently needed. You can find the requirements and instructions for starting a bridge in the [Help Censored Users, Run a Tor Bridge](https://blog.torproject.org/run-a-bridge-campaign/) blog post.

## We need the support of the Internet Freedom community

### Teach users about Tor bridges

Digital security trainers and internet freedom advocates, your help is needed! As this instance of censorship limits direct access to our website, malicious actors could start phishing users with [fake Tor Browsers](https://www.eff.org/deeplinks/2019/10/phony-https-everywhere-extension-used-fake-tor-browser) or spreading disinformation about Tor. Teaching users [how to bypass censorship](https://tb-manual.torproject.org/bridges/) and how to get the official Tor Browser version using [GetTor](https://gettor.torproject.org) or a [mirror](https://tb-manual.torproject.org/downloading/) will be crucial. We need you help spread accurate information about Tor and Tor bridges, particularly among Russian audiences.

### Localize Tor

We have an extremely helpful and responsive Russian translator community, but we urgently need more volunteers. Learn how to [become a Tor translator](https://community.torproject.org/localization/becoming-tor-translator/) and join Tor's [localization IRC channel](https://webchat.oftc.net/?channels=tor-l10n) or use [Element](https://element.io/) to connect to (#tor-l10n:matrix.org).

### Document internet censorship

Russian users can help us see how the Russian government is censoring the internet by running the [OONI probe app](https://ooni.org/install/) on their mobile or desktop devices. OONI, the [Open Observatory of Network Interference](https://ooni.org/), will test if and how Tor is being blocked by your internet provider. After installing, please run the ["Circumvention test"](https://ooni.org/support/ooni-probe-mobile/#measuring-the-blocking-of-circumvention-tools), which will check if circumvention tools like Tor are blocked. Internet measurements are important for detection of anomalous activities; a volunteer running the OONI probe and discussing results with the Tor community was [how we discovered](https://gitlab.torproject.org/tpo/community/support/-/issues/40050) the current censorship in Russia.

### Apply pressure

International digital rights and human rights organizations must pressure Russia's government to immediately revert this censorship.

We will update this post if the situation changes. To receive a notification for updates, you can subscribe to our new [Forum](https://forum.torproject.net) and [click on the bell icon](https://meta.discourse.org/t/email-notifications-for-a-topic/64866).
