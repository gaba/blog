title: New release: Tor Browser 12.5
---
author: duncan
---
pub_date: 2023-06-22
---
categories:
applications
releases
---
summary: 
Tor Browser 12.5 is now available, featuring an updated circuit display, new icons for onion sites, improvements to the connection experience, better accessibility, Finnish language support and more.
---
body:

Tor Browser 12.5 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and our [distribution directory](https://dist.torproject.org/torbrowser/12.5/). Many of the features in this release were made possible thanks to two projects:

Since 2021 we've provided digital security training to [hundreds of journalists and human rights defenders in Brazil, Ecuador and Mexico](https://blog.torproject.org/empowering-human-rights-defenders/) alongside [Guardian Project](https://guardianproject.info/) and [Tails](https://tails.boum.org/). During these workshops we documented pain points with our applications' user experience, and returned to validate potential solutions with Tor Browser Alpha in follow-up trips.

Secondly, in April we [announced the launch of Mullvad Browser](https://blog.torproject.org/releasing-mullvad-browser/), a new privacy browser built by the Tor Project and distributed by [Mullvad](https://mullvad.net/en/browser). This collaboration has enabled us to refactor Tor Browser's build system, address numerous legacy issues and conduct an accessibility review of Tor Browser's custom components – which you can learn more about below.

## What's new?

### Updated circuit display

In Tor Browser for desktop, the [Tor circuit](https://support.torproject.org/glossary/circuit/) for each of your tabs can be found in the circuit display. Up until this release the circuit display lived in the site information panel – meaning you'd have to click the padlock icon (or onion icon, in the case of onion sites) to the left of the address bar to access it. Usability testing participants often struggled to find the circuit display when asked, and users generally needed to be taught where it lived.

To fix this, we've moved the circuit display behind a colorful new icon that sits beside the padlock. In addition, relays now have flags to help make their locations easier to identify at a glance; the design of onion site circuits has been made more concise; [SecureDrop](https://securedrop.org/) users who visit a human-readable onion name can now see and switch back to the underlying V3 onion address; and the panel as a whole has been rebuilt from scratch for better compatibility with screen readers.

![Screenshot of the updated circuit display for securedrop.org in Tor Browser for desktop](125-circuit-display.png)

### New onion site icons

Previously, onion sites were represented by the onion-glyph – a tiny, flat version of Tor Browser's onion logo. Now, when you visit an onion site in Tor Browser 12.5 on either desktop or Android you'll notice something new.

It's important to recognize that onion services are not exclusive to Tor Browser, and are a product in their own right. As [onion service adoption has grown among civil society groups, human rights organizations, and news media outlets](https://blog.torproject.org/how-we-plant-and-grow-new-onions/), so too has support for visiting onion services by third-party apps. Today, in addition to Tor Browser, you can also access onion services in compatible apps like [Orbot](https://orbot.app/en/), [Onion Browser](https://onionbrowser.com/) and [Brave](https://brave.com/), to name a few. Given that, it no longer makes sense to continue to represent onion sites with an icon so closely associated with Tor Browser, and we're excited to introduce their new identity today.

![Screenshot of the ProPublica onion site in Tor Browser for desktop and Android](125-onion-site-icons.png)

### Improved connection experience

In [Tor Browser 10.5 for desktop](https://blog.torproject.org/new-release-tor-browser-105/) we retired the Tor Launcher in favor of a new interface that allows users to connect to Tor from the browser window itself. This feature unlocked the added benefit of being able to access Tor Browser's other menus while offline, including Connection settings, which offers greater functionality than the equivalent Tor Launcher settings page ever did.

Since that release, usability testing participants have sometimes had difficulty figuring out how to connect after navigating away from the Connect to Tor tab. To remedy this, we've made the Connect button accessible in the address bar of any page you visit while offline, and Tor Browser will connect automatically after you've configured a bridge in Connection settings. We've also improved the visibility of the browser's connection status, which can now be found in the top-right of the browser window, and you'll notice a new connection icon appear throughout the browser too.

![Screenshot of the Connection settings tab before connecting in Tor Browser for desktop](125-connection-experience.png)

### Better accessibility

As Tor Browser is based on the Extended Support Release of Firefox, we reuse as much of Firefox's front-end user experience as possible so that we can concentrate our resources on privacy and security issues. However Tor Browser also includes many custom pages and components on top of Firefox.

In parallel with [longstanding efforts by Mozilla to improve Firefox's accessibility](https://blog.mozilla.org/accessibility/), we began an accessibility review of our own to document issues with Tor Browser for desktop's custom features. This review has now been completed, and we're beginning to deploy the first fixes in what will be a multi-release effort to improve Tor Browser's accessibility.

Since Tor Browser 11.5 we've refactored several components including bundled changelogs (about:tbupdate), the circuit display, the security level panel, miscellaneous dialogs and other parts of the browser chrome. If you use a screen reader or any other assistive technology, we'd love to get your help testing past and future fixes by [volunteering as an alpha tester](https://community.torproject.org/user-research/become-tester/) and feeding back to our developers on the [Tor forum](https://forum.torproject.net/c/feedback/tor-browser-alpha-feedback/6).

![Screenshot of the internal changelog with the security level panel expanded in Tor Browser for desktop](125-accessibility.png)

### Finnish language support

In [Tor Browser 12.0 we added support for Albanian and Ukrainian](https://blog.torproject.org/new-release-tor-browser-120/). Now, thanks to the incredible work of our volunteer translators and partners at the [Localization Lab](https://www.localizationlab.org/), we're delighted to include Finnish (Suomi) as a language option on both desktop and Android too. If you spot an error in Finnish or any other language, you can learn more about how to contribute to the translation of Tor Browser, its documentation and our websites on our [localization portal](https://community.torproject.org/localization/).

![Screenshot of the connection page in Finnish in Tor Browser for desktop and Android](125-finnish-support.png)

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/). Thanks to all of the teams across Tor, and the many volunteers, who contributed to this release.

## Full changelog

The full changelog since [Tor Browser 12.0.7](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-12.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - [Bug tor-browser-build#40353](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40353): Re-enable rlbox
  - [Bug tor-browser-build#40810](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40810): Add Finnish (fi) language support
  - [Bug tor-browser#41066](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41066): Circuit Isolation should take containers into account
  - [Bug tor-browser#41428](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41428): Check if we can create our own directories for branding
  - [Bug tor-browser#41514](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41514): eslint broken since migrating torbutton
  - [Bug tor-browser#41568](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41568): Disable LaterRun
  - [Bug tor-browser#41599](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41599): about:networking#networkid should be normalized
  - [Bug tor-browser#41624](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41624): Disable unused about: pages
  - [Bug tor-browser#41635](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41635): Disable the Normandy component at compile time
  - [Bug tor-browser#41636](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41636): Disable back webextension.storage.sync after ensuring NoScript settings won't be lost
  - [Bug tor-browser#41647](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41647): Turn --enable-base-browser in --with-base-browser-version
  - [Bug tor-browser#41662](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41662): Disable about:sync-logs
  - [Bug tor-browser#41671](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41671): Turn media.peerconnection.ice.relay_only to true as defense in depth against WebRTC ICE leaks
  - [Bug tor-browser#41689](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41689): Remove startup.homepage_override_url from Base Browser
  - [Bug tor-browser#41704](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41704): Immediately return on remoteSettings.pollChanges
  - [Bug tor-browser#41738](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41738): Replace the patch to disable live reload with its preference
  - [Bug tor-browser#41763](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41763): TTP-02-003 WP1: Data URI allows JS execution despite safest security level (Low)
  - [Bug tor-browser#41775](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41775): Avoid re-defining some macros in nsUpdateDriver.cpp
  - [Bug tor-browser#41818](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41818): Remove YEC 2022 strings
- Windows + macOS + Linux
  - [Bug mullvad-browser#165](https://gitlab.torproject.org/tpo/applications/mullvad-browser/-/issues/165): Fix maximization warning x button and preference
  - [Bug tor-browser#20497](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/20497): Improve support for non-portable mode
  - [Bug tor-browser#33298](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/33298): HTTP onion sites do not give a popup warning when submitting form data to non-onion HTTP sites
  - [Bug tor-browser#40144](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40144): about:privatebrowsing Firefox branding
  - [Bug tor-browser#40347](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40347): URL bar lock icon says connection is not secure when on "view-source:.onion" URLs [tor-browser]
  - [Bug tor-browser#40552](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40552): New texts for the add a bridge manually modal
  - [Bug tor-browser#40701](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40701): Improve security warning when downloading a file
  - [Bug tor-browser-build#40711](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40711): Review and expand the stakeholders we communicate major changes to
  - [Bug tor-browser-build#40733](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40733): Use the new branding directories
  - [Bug tor-browser-build#40745](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40745): Allow customizing MOZ_APP_BASENAME
  - [Bug tor-browser-build#40773](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40773): Copy some documentation files only on Tor Browser
  - [Bug tor-browser-build#40781](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40781): Move translations to new paths
  - [Bug tor-browser#40788](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40788): Tor Browser 11.0.4-11.0.6 phoning home
  - [Bug tor-browser-build#40808](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40808): Set update URL for nightly base-browser
  - [Bug tor-browser-build#40811](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40811): Make testing the updater easier
  - [Bug tor-browser-build#40817](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40817): Add basebrowser-incrementals-nightly makefile target
  - [Bug tor-browser-build#40833](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40833): base-browser nightly is using the default channel instead of nightly
  - [Bug tor-browser#40958](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40958): The number of relays displayed for an onion site can be misleading
  - [Bug tor-browser#41038](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41038): Update "Click to Copy" button label in circuit display
  - [Bug tor-browser#41080](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41080): Some users are choosing an adjacent country for circumvention settings
  - [Bug tor-browser#41084](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41084): Reserve red as a button color for dangerous actions
  - [Bug tor-browser#41085](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41085): Refactor the UI to remove all bridges
  - [Bug tor-browser#41093](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41093): Users don't understand the purpose of bridge-moji
  - [Bug tor-browser#41109](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41109): "New circuit..." button gets cut-off when onion name wraps
  - [Bug tor-browser#41350](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41350): Move the implementation of Bug 19273 out of Torbutton
  - [Bug tor-browser#41351](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41351): Move the crypto protection patch earlier in the patchset
  - [Bug tor-browser#41363](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41363): Crypto warning popup is not screen reader accessible
  - [Bug tor-browser#41448](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41448): User 'danger' style for primary button in new identity modal
  - [Bug tor-browser#41483](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41483): Tor Browser says Firefox timed out, confusing users
  - [Bug tor-browser#41503](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41503): Disable restart in case of reboot and restore in case of crash
  - [Bug tor-browser#41521](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41521): Improve localization notes
  - [Bug tor-browser#41533](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41533): Page Info window for view-source:http://...onion addresses says Connection Not Encrypted
  - [Bug tor-browser#41540](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41540): Confusing build-id date in about:preferences in alphas
  - [Bug tor-browser#41562](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41562): API-triggered fullscreen after F11 causes letterboxing to crop the page
  - [Bug tor-browser#41577](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41577): Disable profile migration
  - [Bug tor-browser#41587](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41587): Disable the updater for Base Browser
  - [Bug tor-browser#41595](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41595): Disable pagethumbnails capturing
  - [Bug tor-browser#41600](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41600): Some users have difficulty finding the circuit display
  - [Bug tor-browser#41607](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41607): Update "New Circuit" icon
  - [Bug tor-browser#41608](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41608): Improve the UX of the location bar's connection status
  - [Bug tor-browser#41609](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41609): Move the disabling of Firefox Home (Activity Stream) to base-browser
  - [Bug tor-browser#41613](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41613): Skip Drang &amp; Drop filtering for DNS-safe URLs (no hostname, e.g. RFC3966 tel:)
  - [Bug tor-browser#41617](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41617): Improve the UX of the built-in bridges dialog
  - [Bug tor-browser#41618](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41618): Update the iconography used in the status strip in connection settings
  - [Bug tor-browser#41623](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41623): Update connection assist's iconography
  - [Bug tor-browser#41633](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41633): Updating from 12.0.2 to 12.0.3 resets NoScript settings
  - [Bug tor-browser#41657](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41657): Remove --enable-tor-browser-data-outside-app-dir
  - [Bug tor-browser#41668](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41668): Move part of the updater patches to base browser
  - [Bug tor-browser#41686](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41686): Move the 'Bug 11641: Disable remoting by default' commit from base-browser to tor-browser
  - [Bug tor-browser#41695](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41695): Port warning on maximized windows without letterboxing from torbutton
  - [Bug tor-browser#41699](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41699): Tighten up the tor onion alias regular expression
  - [Bug tor-browser#41701](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41701): Reporting an extension does not work
  - [Bug tor-browser#41702](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41702): The connection pill needs to be centered vertically
  - [Bug tor-browser#41709](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41709): sendCommand should not try to send a command forever
  - [Bug tor-browser#41711](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41711): Race condition when opening a new window in New Identity
  - [Bug tor-browser#41718](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41718): Add the external filetype warning to about:downloads
  - [Bug tor-browser#41719](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41719): Update title and button strings in the new circuit display to sentence case
  - [Bug tor-browser#41725](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41725): Stray connectionPane.xhtml patch
  - [Bug tor-browser#41726](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41726): Animate the torconnect icon to transition between connected states
  - [Bug tor-browser#41734](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41734): Add a 'Connected' flag to indicate which built-in bridge option Tor Browser is currently using
  - [Bug tor-browser#41736](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41736): Customize the default CustomizableUI toolbar using CustomizableUI.jsm
  - [Bug tor-browser#41749](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41749): Replace the onion-glyph with dedicated icon for onion services
  - [Bug tor-browser#41770](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41770): Keyboard navigation broken leaving the toolbar tor circuit button
  - [Bug tor-browser#41775](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41775): Avoid re-defining some macros in nsUpdateDriver.cpp
  - [Bug tor-browser#41785](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41785): Network monitor in developer tools shows HTTP onion resources as insecure
  - [Bug tor-browser#41792](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41792): Drag and Drop protection prevents dragging downloads
  - [Bug tor-browser#41800](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41800): Add the external filetype warning to Library / Manage Bookmarks
  - [Bug tor-browser#41801](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41801): Fix handleProcessReady in TorSettings.init
  - [Bug tor-browser#41802](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41802): Bad regex used to extract transport from bridgeline
  - [Bug tor-browser#41810](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41810): Add "Connect" buttons to Request Bridge and Provide Bridge modals
  - [Bug tor-browser#41816](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41816): The top navigation in about:torconnect isn't updated correctly
  - [Bug tor-browser#41841](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41841): Use the new onion-site.svg icon in the onion-location pill
- Windows + Linux
  - [Bug tor-browser-build#40714](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40714): Ship NoScript in the distribution directory also for Windows and Linux
  - [Bug tor-browser#41654](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41654): UpdateInfo jumped into Data
- Windows
  - [Bug tor-browser-build#40772](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40772): Check and fix HiDPI issues in the NSIS installer
  - [Bug tor-browser-build#40793](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40793): Add some metadata also to the Windows installer
  - [Bug tor-browser-build#40801](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40801): Correct the ExecShell for system-wide installs in the NSIS script
  - [Bug tor-browser#41459](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41459): WebRTC fails to build under mingw
  - [Bug tor-browser#41678](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41678): WebRTC build fix patches incorrectly defining pid_t
- macOS
  - [Bug tor-browser-build#40719](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40719): Allow non-universal macOS builds also on base-browser
  - [Bug tor-browser#41535](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41535): Remove the old, unused and undocumented "-invisible" macOS CLI flag
- Linux
  - [Bug tor-browser-build#40830](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40830): The fontconfig directory is missing in Base Browser
  - [Bug tor-browser-build#40860](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40860): Improve the transition from the old fontconfig file to the new one
  - [Bug tor-browser#41163](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41163): Many bundled fonts are blocked in Ubuntu/Fedora because of RFP
  - [Bug tor-browser#41732](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41732): implement linux font whitelist as defense-in-depth
- Android
  - [Bug tor-browser#41001](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41001): Remove remaining security slider code
  - [Bug tor-browser#41185](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41185): Hide learn more about sync
  - [Bug tor-browser#41634](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41634): Google Play incorrectly detects that libTor.so is built with OpenSSL 1.1.1b
  - [Bug tor-browser#41667](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41667): Enable media.peerconnection.ice.obfuscate_host_addresses on Android for defense-in-depth
  - [Bug tor-browser#41677](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41677): Remove the --disable-tor-browser-update flag on Android
- Build System
  - All Platforms
    - Updated Go to 1.20.5
    - [Bug tor-browser-build#40673](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40673): Avoid building each go module separately
    - [Bug tor-browser-build#40679](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40679): Use the latest translations for nightly builds
    - [Bug tor-browser-build#40689](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40689): Update Ubuntu version from projects/mmdebstrap-image/config to 22.04.1
    - [Bug tor-browser-build#40717](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40717): Create a script to prepare changelogs
    - [Bug tor-browser-build#40720](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40720): Update fetch-changelogs.py scripts to support new Build System label
    - [Bug tor-browser-build#40750](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40750): Find why rlbox hurts reproducibility
    - [Bug tor-browser-build#40751](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40751): make signtag-* needs to take project name into account
    - [Bug tor-browser-build#40753](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40753): We should not copy mar tools when the updater is disabled
    - [Bug tor-browser-build#40760](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40760): Add BSD packager contacts to release prep templates
    - [Bug tor-browser-build#40763](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40763): Add support for signing multiple browsers in tools/signing/nightly
    - [Bug tor-browser-build#40783](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40783): Update download-unsigned-sha256sums-gpg-signatures-from-people-tpo to use $projectname prefix directory
    - [Bug tor-browser-build#40784](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40784): Fix var_p/nightly_torbrowser_incremental_from after #40737
    - [Bug tor-browser-build#40794](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40794): Include the build-id in firefox-l10n output name
    - [Bug tor-browser-build#40795](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40795): Trim down tor-browser-build release prep issue templates
    - [Bug tor-browser-build#40796](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40796): Bad UX for the changelogs script when using the issue number
    - [Bug tor-browser-build#40805](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40805): Define the version flag for all browsers
    - [Bug tor-browser-build#40807](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40807): Add config for signing base-browser nightly in tools/signing/nightly
    - [Bug tor-browser-build#40812](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40812): Make var/rezip in projects/firefox/config quiet
    - [Bug tor-browser-build#40818](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40818): Enable wasm target for rust compiler
    - [Bug tor-browser-build#40828](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40828): Use http://archive.debian.org/debian-archive/ for jessie
    - [Bug tor-browser-build#40837](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40837): Rebase mullvad-browser build changes onto main
    - [Bug tor-browser-build#40870](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40870): Remove url without browser name from tools/signing/download-unsigned-sha256sums-gpg-signatures-from-people-tpo
    - [Bug tor-browser#41649](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41649): Create rebase and security backport gitlab issue templates
    - [Bug tor-browser#41682](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41682): Add base-browser nightly mar signing key
  - Windows + macOS + Linux
    - [Bug tor-browser-build#33953](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/33953): Provide a way for easily updating Go dependencies of projects
    - [Bug tor-browser-build#40713](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40713): Use the new tor-browser l10n branch in Firefox
    - [Bug tor-browser-build#40777](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40777): Create a Go bootstrap project
    - [Bug tor-browser-build#40778](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40778): Disable all translations with testbuilds in Firefox
    - [Bug tor-browser-build#40788](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40788): Remove all languages but en-US for privacy-browser build target
    - [Bug tor-browser-build#40809](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40809): Remove --enable-tor-browser-update and --enable-verify-mar from projects/firefox/mozconfig
    - [Bug tor-browser-build#40813](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40813): Enable var/updater_enabled for basebrowser nightly
    - [Bug tor-browser-build#40823](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40823): Update appname_* variables in projects/release/update_responses_config.yml
    - [Bug tor-browser-build#40826](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40826): Correctly set appname_marfile for basebrowser in tools/signing/nightly/update-responses-base-config.yml
    - [Bug tor-browser-build#40827](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40827): MAR generation uses (mostly) hard-coded MAR update channel
    - [Bug tor-browser-build#40841](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40841): Adapt signing scripts to new signing machines
    - [Bug tor-browser-build#40849](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40849): Move Go dependencies to the projects dependent on them, not as a standalone projects
    - [Bug tor-browser-build#40866](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40866): Remove `Using ansible to set up a nightly build machine` from README
    - [Bug tor-browser-build#40869](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40869): obfs4 is renamed to lyrebird
  - Windows
    - [Bug tor-browser-build#29185](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/29185): NSIS Installer not reproducible when icon has an alpha channel
    - [Bug tor-browser-build#40757](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40757): Change projects/browser/windows-installer/torbrowser.nsi to a template file
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40732](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40732): Review Bundle-Data and try not to ship the default profile in base browser
  - Linux + Android
    - [Bug tor-browser-build#40653](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40653): Build compiler-rt with runtimes instead of the main LLVM build
  - macOS
    - [Bug tor-browser-build#40792](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40792): signing scripts missing project name prefix to make rule
    - [Bug tor-browser-build#40798](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40798): dmg2mar step also takes care of copying the signed+stabled dmg to the signed directory
    - [Bug tor-browser-build#40806](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40806): Update the reference to the macOS mozconfig
    - [Bug tor-browser-build#40824](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40824): dmg2mar script using hardcoded project names for paths
    - [Bug tor-browser-build#40847](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40847): Build filesystem influences the DMG creation
    - [Bug tor-browser-build#40858](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40858): Create script to assist testers self sign Mac builds to allow running on Arm processors
    - [Bug tor-browser#41453](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41453): Rename mozconfig-macos-x86_64 to mozconfig-macos
  - Android
    - [Bug tor-browser-build#40738](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40738): Update Android git hashes templates
    - [Bug tor-browser-build#40874](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40874): Add commit information also to GV
    - [Bug tor-browser#41684](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41684): Android improvements for local dev builds
