title: Arti 0.0.4 is released: Refactoring, rustls, and more!
---
author: nickm
---
pub_date: 2022-02-01
---
categories: announcements
---
summary:
Arti 0.0.4 is released, and available for download.
---
body:


Arti is our ongoing project to create a working embeddable Tor client in Rust. It’s nowhere near ready to replace the main Tor implementation in C, but we believe that it’s the future.

We're working towards our 0.1.0 milestone in early March, where our main current priorities are stabilizing our APIs, and resolving issues that prevent integration.  We're planning to do releases every month or so until we get to that milestone.

Please be aware that _every_ release between now and then will probably break backward compatibility.

# So, what's new in Arti 0.0.4?

There are new features in our API for developers who need to isolate all circuits from one another, and for setting per-stream preferences across all streams opened through a client handle.  The interfaces for creating and using the main `TorClient` type are also improved.  We've also refactored a large amount of our internal code for creating and dealing with asynchronous runtimes, and added support for using [`rustls`](https://github.com/rustls/rustls).  (Our default TLS library is still [`native_tls`](https://docs.rs/native-tls/latest/native_tls/).)

In the background, we've also got a background for reporting bootstrap status.  It isn't too useful yet, since the current API doesn't actually give you anyway to _get_ a not-yet-bootstrapped `TorClient`, but we hope to change that in the future.

There are also a bunch of smaller features, bugfixes, and infrastructure improvements; see the [changelog](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md) for a more complete list.

# And what's next?

Between now and March, we're going to be focused on three kinds of improvements:

  * Those that will require [API breakage](https://gitlab.torproject.org/tpo/core/arti/-/issues?scope=all&state=opened&label_name[]=API%20Break).
  * Those based on [developer feedback](https://gitlab.torproject.org/tpo/core/arti/-/issues?scope=all&state=opened&label_name[]=User%20Feedback).
  * Those that resolve [technical debt](https://gitlab.torproject.org/tpo/core/arti/-/issues?scope=all&state=opened&label_name[]=Technical%20Debt).



# Here's how to try it out

We rely on users and volunteers to find problems in our software and suggest directions for its improvement.  Although Arti isn't yet ready for production use, you can test it as a SOCKS proxy (if you're willing to compile from source) and as an embeddable library (if you don't mind a little API instability).

Assuming you've installed Arti (with `cargo install arti`, or directly from a cloned repository), you can use it to start a simple SOCKS proxy for making connections via Tor with:

```
$ arti proxy -p 9150
```

and use more or less as you would use the C Tor implementation!

(It doesn't support onion services yet.  If compilation doesn't work, make sure you have development files for libsqlite installed on your platform.)

For more information, check out the [README](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md) file. (For now, it assumes that you're comfortable building Rust programs from the command line).  Our [CONTRIBUTING](https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CONTRIBUTING.md) file has more information on installing development tools, and on using Arti inside of Tor Browser. (If you want to try that, please be aware that Arti doesn't support onion services yet.)

When you find bugs, please report them [on our bugtracker](https://gitlab.torproject.org/tpo/core/arti/).  You can [request an account](https://anonticket.torproject.org/) or [report a bug anonymously](https://anonticket.torproject.org/).

And if this documentation doesn't make sense, please ask questions! The questions you ask today might help improve the documentation tomorrow.

# Call for comments—Urgent!

We need feedback on our APIs.  Sure, _we_ think we're making them more complete and ergonomic… but it's the users' opinion that matters!

Here are some ideas of how you can help:

1. You can read over the [high-level APIs](https://tpo.pages.torproject.net/core/doc/rust/arti_client/index.html) for the `arti-client` crate, and look for places where the documentation could be more clear, or where the API is ugly or hard to work with.

2. Try writing more code with this API: what do you wish you could do with Tor in Rust?  Give it a try! Does this API make it possible?  Is any part of it harder than necessary?  (If you want, maybe clean up your code and contribute it as an [example](https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti-client/examples)?)

# Acknowledgments

Thanks to everybody who has contributed to this release, including Arturo Marquez, Daniel Eades, Daniel Schischkin, Jani Monoses, Neel Chauhan, and Trinity Pointard.


And thanks, of course, to [Zcash Open Major Grants (ZOMG)](https://grants.zfnd.org/proposals/215972995-arti-a-pure-rust-tor-implementation-for-zcash-and-beyond) for funding this project!
