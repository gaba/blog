title: All donations to the Tor Project matched 1:1, now through Dec 31
---
author: alsmith
---
pub_date: 2024-10-14
---
categories:

fundraising
---
summary: Tor helps people to browse, search, and speak with freedom. Join the thousands of supporters in building an internet powered by privacy. [Make a donation today](https://torproject.org/donate/donate-bp1-yec2024).
---
body:

Each year during this season, the Tor Project holds a fundraiser during which we ask for your support. We do this because the Tor Project is a nonprofit organization, powered by donations from our community. Donations make it possible for the Tor Project to build tools powered by people—not profit.

Over the next few months, we’ll be sharing stories from some of the millions of people you’re helping when you support Tor, the impact we've made this year, details about what’s coming next to our suite of privacy and censorship circumvention tools, and ways you can help make privacy online easy and accessible.

Now is a great time to give and spread the word about the Tor Project because **[through the end of the year, all donations will be matched](https://torproject.org/donate/donate-bp1-yec2024)** by our supporters at [Power Up Privacy](https://powerupprivacy.com/). That means when you donate $25, you’re making a $50 impact. Plus, we’ve introduced a brand-new item to our list of gifts you can receive in return for making a donation. 👀

![A picture of a black cap with an embroidered purple onion](hat.png)

Below is a short preview of the events we’re hosting for you from now until the end of the year. We hope you will save these dates and choose a way to contribute or spread the word about the Tor Project today!

## Upcoming events

* **Global Encryption Day: Distribute(d) trust – The key to global encryption access (Monday, October 21)**
   * For many vulnerable Internet users, the Tor network is the only way to gain access to encrypted services. It's made possible by relay operators, the thousands of volunteers who donate their time, technical expertise, and hardware. Join us for a roundtable discussion with individual and institutional relay operators to learn more about the ins and outs of powering a safer, more equitable internet. The virtual live event is part of Tor's contributions to Global Encryption Day.
   * 📺 Stream live on our [YouTube channel](https://www.youtube.com/@TheTorProject)

* **State of the Onion – the Tor Project (Wednesday, November 13)**
   * The State of the Onion is the Tor Project's annual virtual event where we share updates from the Tor Project and the Tor community. The event on November 13 will focus on the Tor Project and the organization’s work.
   * 📺 Stream live on our [YouTube channel](https://www.youtube.com/@TheTorProject)

* **State of the Onion – Community (Wednesday, November 20)**
   * The State of the Onion is the Tor Project's annual virtual event where we share updates from the Tor Project and the Tor community. The event on November 20 will focus on organizations in the Tor community and their work.
   * 📺 Stream live on our [YouTube channel](https://www.youtube.com/@TheTorProject)

* **Human Rights Day (Tuesday, December 10)**
   * Every year on December 10, we celebrate the UN’s adoption and proclamation of the[ Universal Declaration of Human Rights](https://en.wikipedia.org/wiki/Universal_Declaration_of_Human_Rights) (UDHR) in 1948. The UDHR outlines privacy as one of the human rights everyone should be free to exercise.
   * 📎 Keep an eye on the [blog](blog.torproject.org) and [newsletter](https://newsletter.torproject.org) for event updates.

## Ways to contribute

* **Make a donation:** [Donate through our website](https://torproject.org/donate/donate-bp1-yec2024) (or any other method listed on our [FAQ](https://donate.torproject.org/faq)) and your donation will be matched, 1:1, up to $300,000.
* **Ask the company you work for if they will match your donation:** Many corporations will match their employees’ donations to charitable organizations. Ask at work if your company will match your gift.
* **Share on social media:** Let the people in your networks know that all donations to the  Tor Project are currently being matched. You can easily share a post from our social channels: [Mastodon](https://mastodon.social/@torproject), [Bluesky](https://bsky.app/profile/torproject.org), [X](https://x.com/torproject), and more.
* **Subscribe to Tor News:** No ads. No tracking. Just [low-traffic Tor updates via email.](https://newsletter.torproject.org)
