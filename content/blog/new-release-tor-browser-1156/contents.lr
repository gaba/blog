title: New Release: Tor Browser 11.5.6 (Android, Windows, macOS, Linux)
---
pub_date: 2022-10-27
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.6 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.6/).

This is an emergency release resolving an issue with Tor Browser 11.5.5's integration of the Snowflake pluggable transport. Users of 11.5.5 will be unable to connect to the Tor Network via the built-in Snowflake bridge until they update to 11.5.6.

If you applied [the following workaround to add a working Snowflake bridge manually](https://github.com/net4people/bbs/issues/131#issuecomment-1291070401), you may revert back to using the "Built-in" Snowflake bridge option after updating to 11.5.6.

The full changelog since [Tor Browser 11.5.5](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.5) is:

- All Plaforms
  - [Bug tor-browser-build#40665](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40665): Shorten snowflake bridge line
