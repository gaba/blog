title: Double your donation now! All gifts matched 1:1, up to $75,000
---
author: alsmith
---
pub_date: 2023-11-27
---
categories: fundraising
---
summary: **Starting today through December 31, your donation to the Tor Project will be matched 1:1.** That means that every donation, up to $75,000, will be doubled. Now is an excellent time to maximize the power of your [gift and make your commitment to privacy online](https://torproject.org/donate/donate-bp-fot-2023).
---
body:

**Starting today through December 31, your donation to the Tor Project will be matched 1:1.** That means that every donation, up to $75,000, will be doubled. Now is an excellent time to maximize the power of your [gift and make your commitment to privacy online](https://torproject.org/donate/donate-bp-fot-2023).

This match is made possible by the Friends of Tor, an outstanding group of contributors who have made the commitment to advancing privacy online. We're very thankful to the leadership of the Friends of Tor.

No matter where you make your gift-via [website](https://torproject.org/donate/donate-bp-fot-2023), [mail](https://donate.torproject.org/donor-faq/#can-i-donate-with-a-check-cash-or-money-order), or [cryptocurrency](https://donate.torproject.org/cryptocurrency)-your donation will be matched. Your gift will go a long way in [supporting those who need it the most](https://blog.torproject.org/support-tor-project-share-your-story/): whether it's enabling censorship circumvention, empowering political organizing, curbing surveillance, facilitating research and journalism, or protecting whistleblowers.

[![Donate Button](yec-donate-button.png "class=align-center")](https://torproject.org/donate/donate-bp-fot-2023)

## Learn about the Friends of Tor who have made this opportunity possible

![Aspiration logo](aspiration-logo-2023-small.png "class=align-left") **[Aspiration](https://aspirationtech.org)** connects nonprofit organizations, foundations and activists with free and open software solutions and technology skills that help them better carry out their missions. We want those working for social and racial justice to be able to find and use the best tools and practices available, so that they maximize their effectiveness and impact and, in turn, change the world. We also work with free and open source projects and communities in both [support and partnership roles](https://aspirationtech.org/services/openprojects), advising and contributing on matters of strategy, sustainability, governance, community health, equity and diversity. We design and facilitate unique and collaborative nonprofit and FLOSS technology [convenings](https://aspirationtech.org/events), and have run almost 800 in over 50 countries as well as online over the past 16 years. Our [Strategic Development Program](https://aspirationtech.org/programs/strategicdevelopment/) helps nonprofit leaders and fundraisers take a wholly new and empowering approach to revenue development, and our [Policy Leadership Initiative](https://aspirationtech.org/programs/policyleadership) supports emerging leaders in the European policy space as they demystify and engage with an opaque system.

----

![DEF CON logo](defcon-logo-2023-small.png "class=align-left") **[DEF CON](https://defcon.org/)** is a hacking conference started in 1993 that has grown and adapted with the times. With almost 30k people attending the event yearly DEF CON stays true to its core by sharing its content for free and operating services for the community such as the DEF CON Forums (now over 20 years operating), our Mastodon server DEFCON.social, Tor .onion servers and relays, as well as a discord.gg/defcon. People gather at DEF CON Groups around the world to share and learn. DEF CON believes in the power of the idea, how you conduct yourself, and not what you look like or how hard you meme. Supporting and operating services with Tor is an investment in a more open and accessible future. Get involved!

----

![Portrait of Christopher Seiwald](christopher-seiwald-2023-small.png "class=align-left") **Christopher Seiwald** spent most of his adult life pursuing one software project or another, culminating with founding and running Perforce Software for 21 years. Ever thankful for the communities which helped him along, in his post-CEO life he has focused on giving back in the form of angel investing, philanthropy, and leading the charge to build a performing arts theater in his hometown of Alameda, California. In addition to being a Tor advocate, he's a longtime supporter of Wikimedia and the Signal Foundation.

----

![Portrait of Craig Newmark](craig-newmark-photo-2023-small.png "class=align-left") **[Craig Newmark](https://craignewmarkphilanthropies.org)** is best known as the founder of the classifieds ad site craigslist, which showed tens of millions of Americans that the Internet could be reasonably useful and easy to use. Now, he engages in full time philanthropy, focusing on helping and protecting the people who help and protect our country. That includes cybersecurity, trustworthy journalism, and support for military families and veterans.

----

![Portrait of Jon Callas](jon-callas-photo-2023-small.png "class=align-left") **[Jon Callas](https://mastodon.social/@joncallas)** is a cryptographer, software engineer, user experience designer, and entrepreneur. Jon is the co-author of many crypto and security systems including OpenPGP, DKIM, ZRTP, Skein, and Threefish. Jon has co-founded several startups including PGP, Silent Circle, and Blackphone. Jon has worked on security, user experience, and encryption for Apple, Kroll-O'Gara, Counterpane, and Entrust. Jon has also been a public interest technologist at the ACLU and EFF on issues including surveillance, encryption, machine learning, end-user security, and privacy. Jon is fond of Leica cameras, Morgan sports cars, and Birman cats. Jon's photographs have been used by Wired, CBS News, and the Guggenheim Museum.

----

![Portrait of Tamzen Cannoy](tamzen-cannoy-2023-small.png "class=align-left") **Tamzen Cannoy** is a user experience designer and entrepreneur. Tamzen was CEO and founder of World Benders, the first company to build software for meetings on the internet. She has worked for Digital Equipment Corporation, TGV, Cisco, and PGP. Tamzen's hobbies include modern gastronomy experimentation, fabric arts, and holding cats on her shoulder.

----

![Portrait of Matt Blaze](matt-blaze-photo-2023-small.png "class=align-left") **[Matt Blaze, Ph.D.](https://www.mattblaze.org/)**, is professor of law at Georgetown Law and professor of computer science at Georgetown University. For more than 25 years, Professor Blaze's research and scholarship has focused on security and privacy in computing and communications systems, especially as we rely on insecure platforms such as the internet for increasingly critical applications. His work has focused particularly on the intersection of this technology with public policy issues. For example, in 2007, he led several of the teams that evaluated the security of computerized election systems from several vendors on behalf of the states of California and Ohio.

----

![Portrait of Wendy Seltzer](wendy-seltzer-2023-small.png "class=align-left") **[Wendy Seltzer](https://wendy.seltzer.org)** is Principal Identity Architect with Tucows, working toward the privacy-respecting governance of identity systems. As a Fellow with Harvard's Berkman Klein Center for Internet & Society, Wendy founded the Lumen Database, the web's pioneering transparency report to measure the impact of legal takedown demands online. She seeks to improve technology policy in support of user-driven innovation and secure communication.

----

We would also like to thank the **anonymous donors** who contributed to this match. This spot is dedicated to them as a recognition of their support. As the Tor community knows, anonymity loves company; [why not join our anonymous donors and make a contribution?](https://torproject.org/donate/donate-bp-fot-2023) With their matching donation, your contribution has double the impact.