title: Arti 1.2.1 is released: onion services development
---
author: nickm
---
pub_date: 2024-04-02
---
categories: announcements
---
summary:

Arti 1.2.1 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.2.1.

This release continues development on onion services,
and adds several important security features.
[More such improvements] are on the way.
See [`doc/OnionService.md`] for instructions and caveats about running
onion services with Arti today.

This release also adds support for
[unmanaged pluggable transports][#755].

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Alexander Færøy, Jim Newsome, Tobias Stoeckmann, and trinity-1686a.

Also, our deep thanks to [Zcash Community Grants] and our [other sponsors]
for funding the development of Arti!

[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md?ref_type=heads
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[other sponsors]: https://www.torproject.org/about/sponsors/

[More such improvements]: https://gitlab.torproject.org/tpo/core/arti/-/issues/?label_name%5B%5D=Onion%20Services%3A%20Improved%20Security
[#755]: https://gitlab.torproject.org/tpo/core/arti/-/issues/755
[`doc/OnionService.md`]: https://gitlab.torproject.org/tpo/core/arti/-/blob/arti-v1.2.1/doc/OnionService.md

