title: New Release: Tor Browser 12.0.6
---
author: richard
---
pub_date: 2023-05-12
---
categories:
applications
releases
---
summary: Tor Browser 12.0.6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:

Tor Browser 12.0.6 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also
from our [distribution directory](https://dist.torproject.org/torbrowser/12.0.6/).

This release updates Firefox to 102.11esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-17/). There were no Android-specific security updates to backport from the Firefox 113 release.

## Build-Signing Infrastructure Updates

We are in the process of updating our build signing infrastructure, and unfortunately are unable to ship code-signed 12.0.6 installers for Windows systems currently. Therefore we will not be providing full Window installers for this release. However, automatic build-to-build upgrades from 12.0.4 and 12.0.5 should continue to work as expected.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 12.0.5](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-12.0/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated Go to 11.9.9
  - [Bug tor-browser#41728](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41728): Pin bridges.torproject.org domains to Let's Encrypt's root cert public key
  - [Bug tor-browser#41756](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41756): Rebase Tor Browser Stable to 102.11.0esr
- Windows + macOS + Linux
  - Updated Firefox to 102.11esr
  - [Bug tor-browser#40501](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40501): High CPU load after tor exits unexpectedly
- Windows
  - [Bug tor-browser#41683](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41683): Disable the network process on Windows
- Android
  - Updated GeckoView to 102.11esr
- Build System
  - Windows + macOS + Linux
    - [Bug tor-browser#41730](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41730): Bridge lines in tools/torbrowser/bridges.js out of date
  - macOS
    - [Bug tor-browser-build#40844](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40844): Fix DMG reproducibility problem on 12.0.5
