title: The Results Are In: Furthering Our Mission In The Global South
---
author: pavel
---
pub_date: 2023-03-01
---
categories:

community
reasearch
human rights
usability
global south

---
summary: We have conducted an assessment of our strategy to make Tor services more broadly available in the Global South to better understand our progress and potential for improvement.
---
body:

In 2017, the Tor Project developed and launched its Global South Strategy (GSS) which was conceived as a means to [establish closer ties and build better relationships with at-risk communities](https://blog.torproject.org/community-portal/) in the Global South and to understand how we can [remove common barriers to the adoption of our services](https://blog.torproject.org/reaching-people-where-they-are/). The goal is to empower people to be more secure on the internet by prioritizing a user-centric approach for the development of Tor technologies and products.

Following the success of the first five years of the program, we engaged Firetail, a strategy consultancy working to achieve social progress, to conduct a thorough assessment of our approach. We wanted to track and measure progress towards the above goals. Today, we are pleased to share this evaluation report, its findings and recommendations with our community.

We would love to take the opportunity to thank all of our community members, partners and supporters for all the resources that they have dedicated. Special thanks to [Sida](https://www.sida.se/en), The Swedish International Development Cooperation Agency, that has sponsored the strategy development.

**You can find the full evaluation report further [here.](The Tor Project-GSS Evaluation-for-sharing.pdf) But for the TL;DR you can read the rest of this post.**

## What's The Significance of Tor's Global South Strategy?

-   Journalists, activists, human rights defenders, and at-risk communities face increasing censorship, surveillance, and prosecution, particularly in East Africa and Latin America.
-   While the Tor Project offers a variety of safe anonymity services and products, accessibility, [usability, awareness and education](https://blog.torproject.org/strength-numbers-usable-tools-dont-need-be-invasive/) have historically been a challenge to at-risk communities in the Global South.
-   That's why the GSS consists of four, interconnected program components: (1) Improving the usability of Tor knowledge resources, (2) capturing and communicating usability issues, (3) in-country outreach to conduct training events, (4) and building better software and scaling the programme -- all of which have been evaluated

## What Were The Key Findings?

-   As part of its GSS, Tor was able to shift perception from an academic/hacker organization to one that is more community-focused. It has become the connective tissue between developers and users on the ground. However, there is an opportunity to communicate the GSS more coherently and consistently to internal teams.
-   The Tor Project can scale the GSS through better formalization and strategic partnerships, and there is a general consensus among stakeholders that Tor tools have improved and knowledge resources have become more accessible for Global South users since the launch of the program.
-   The GSS has strengthened partners' capabilities and supported their work to further their missions. Concrete examples include accessing blocked information, overcoming censorship and empowering minority groups and at-risk communities. However, there is an ongoing need for GSS activities in the future, especially when it comes to reaching members in less concentrated regions.

## What's Next?

-   The GSS has brought Tor staff closer to its users in the Global South, and supported marked improvements in Tor tools. The UX team has demonstrated that ethical, transparent, and non-invasive data collection from users is possible. External stakeholders report that Tor is responsive to user feedback.
-   There is an ongoing need for GSS activities, and the project should consider better tailoring and teaching of more technical tools to non-technical audiences. The GSS should also use user personas to drive the structure and focus areas of the training sessions, with opportunities for partnering with peer organizations. Systematic feedback from the UX team to the developers and other relevant teams could be improved to support the cadence for product development.
-   The GSS is a critical strategy for the Tor Project in connecting with its Global South users, particularly those in regions with increasing challenges to [digital rights](https://blog.torproject.org/digital-rights-are-human-rights/). While there have been marked improvements in Tor tools and the organization's response to user feedback, there is a need for better coordination within the community and more consistency in internal as well as external communication.

## Increasing Reach and Improving Relevance

We have set those key findings in motion and are excited to advance into the next phase of this project. It will involve building and iterating on the successful building blocks to foster proactive outreach, to facilitate networking among like-minded people and to capitalize on opportunities to grow the community by targeting more underserved communities

Stay tuned for more updates.
