title: New Release: Tor Browser 14.0.2
---
pub_date: 2024-11-13
---
author: morgan
---
categories:

applications
releases
---
summary: Tor Browser 14.0.2 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 14.0.2 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/14.0.2/).

This version fixes a tor crash bug on macOS when attempting to visit onion sites with PoW protections enabled ([tor-browser#43245](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43245))

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 14.0.1](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-14.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated NoScript to 11.5.2
  - [Bug tor-browser#43257](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43257): NoScript-blocked content placeholders causing slow downs
- Windows + macOS + Linux
  - [Bug tor-browser#32668](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/32668): NoScript default whitelist re-appears on clicking NoScript Options / Reset
  - [Bug tor-browser#43258](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43258): NoScript Lifecycle error on extension updates
  - [Bug tor-browser#43262](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43262): Onion keys dialog. "Remove" removes all keys, "Remove all" does nothing.
- macOS
  - [Bug tor-browser#43245](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43245): TB14 on macOS crashing when visiting some onionsites
- Build System
  - All Platforms
    - Updated Go to 1.22.9
  - Windows + macOS + Linux
    - [Bug tor-browser-build#41286](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41286): Update the deploy update scripts to optinally take an override hash
