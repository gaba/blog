title: New Release: Tor Browser 13.0.6 (Desktop)
---
pub_date: 2023-12-05
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0.6 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0.6 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0.6/).

This is an unscheduled release to fix a crash bug affecting users running Tor Browser using Wayland. Please see [tor-browser#42306](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42306) for more information.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.0.5](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.0/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - [Bug tor-browser#42288](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42288): Allow language spoofing in status messages
- Windows + macOS + Linux
  - [Bug tor-browser#42302](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42302): The allowed ports string contains a typo
  - [Bug tor-browser#42231](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42231): Improve the network monitor patch for http onion resources
  - [Bug tor-browser#42299](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42299): After adding incorrect bridge addres on user cannot go back to the Connection page
- Linux
  - [Bug tor-browser#17560](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/17560): Downloaded URLs disk leak on Linux
  - [Bug tor-browser#42306](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42306): Tor Browser crashes when extensions popups are opened with Wayland enabled
  - [Bug tor-browser-build#41017](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41017): Disable Nvidia shader cache
- Build System
  - All Platforms
    - [Bug tor-browser-build#41027](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41027): Remove tb-build-04 and tb-build-05 from tools/signing/download-unsigned-sha256sums-gpg-signatures-from-people-tpo
    - [Bug tor-browser-build#40936](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40936): Revert tor-browser-build#40933
    - [Bug tor-browser-build#40995](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40995): Use cdn.stagemole.eu instead of cdn.devmole.eu in download-unsigned-sha256sums-gpg-signatures-from-people-tpo
    - [Bug rbm#40064](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40064): Using exec on project with no git_url/hg_url is causing warning
  - Windows + macOS + Linux
    - [Bug tor-browser-build#41031](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41031): Add command to unsign .mar files and compare with sha256sums-unsigned-build.txt
  - Windows
    - [Bug tor-browser-build#41030](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41030): Add command to unsign .exe files and compare with sha256sums-unsigned-build.txt
  - Android
    - [Bug tor-browser-build#41024](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41024): Fix android filenames in Release Prep issue templates
