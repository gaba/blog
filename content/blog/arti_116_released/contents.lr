title: Arti 1.1.6 is released: Now you can connect* to Onion Services!
---
author: nickm
---
pub_date: 2023-06-30
---
categories: announcements
---
summary:

Arti 1.1.6 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.1.6.

After months of hard work,
Arti finally has working
client-side onion service support!
That is, programs can\* now use Arti
to connect to onion services on the Tor network.

\*: Note that this feature is _not yet as secure_ as the
equivalent feature in the C tor implementation,
and as such you probably shouldn't use it
for security-sensitive purposes.
(Our implementation is missing the “[vanguards-lite]” feature
that C tor uses to prevent [guard discovery] attacks.)
For this reason, the feature is (for now) disabled by default.
To turn it on, you can enable it on the command line
(`arti -o address_filter.allow_onion_addrs=true proxy`)
or edit your `arti.toml` configuration file
(set `allow_onion_addrs = true` in the section `[address_filter]`).

(*Edited 2023-07-07 to add*: Also, when you build Arti,
you need to provide a non-default Cargo feature.
Add `--features=arti/onion-service-client` when building.
This restriction will be removed in the next release.)

This release also introduces our key manager functionality.
Unlike the C `tor` implementation,
where the ability to manage keys on disk
grew organically (and unevenly) over time,
with Arti we’re trying to provide
a uniform and consistent API and CLI
for managing secret keys.
For now, this functionality is in a preliminary state,
and the usability is somewhat lacking.
If you want, you can use it to experiment with
[onion service client authorization],
but you might have a better time
if you wait until the next release.

With this release, there have been
many smaller and less visible changes as well;
for those, please see the [CHANGELOG].

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Alexander Færøy, Andy, Jim Newsome, nate\_d1azzz, pinkforest,
Saksham Mittal, and Trinity Pointard.

Finally, our deep thanks to [Zcash Community Grants] for funding the
development of Arti!


[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-116-30-june-2023
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[vanguards-lite]: https://gitlab.torproject.org/tpo/core/torspec/-/blob/main/proposals/333-vanguards-lite.md
[guard discovery]: https://blog.torproject.org/announcing-vanguards-add-onion-services/
[onion service client authorization]: https://gitlab.torproject.org/gabi-250/arti-client-auth