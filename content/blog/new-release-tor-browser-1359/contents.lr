title: New Release: Tor Browser 13.5.9
---
pub_date: 2024-10-28
---
_discoverable: no
---
author: morgan
---
categories:

applications
releases
---
summary: Tor Browser 13.5.9 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.5.9 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.5.9/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/) to Firefox.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 13.5.8](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-13.5/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

 - Windows + macOS + Linux
  - Updated Tor to 0.4.8.13
  - [Bug tor-browser#43240](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43240): Backport security fixes from Firefox 132
  - [Bug tor-browser-build#41273](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41273): relprep.py: bump Firefox and GV to a (yet) non-existing tag when the last one does not match HEAD
  - Updated Firefox to 115.17.0esr
  - [Bug tor-browser#42280](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42280): Weird connection attempt to multicast IPv6 ff00:::443 on "New identity"
  - [Bug tor-browser#43104](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43104): Local files and extensions can't be loaded in new windows before bootstrap
  - [Bug tor-browser#43169](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43169): compat: align userAgent in navigator + HTTP Header
  - [Bug tor-browser#43174](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43174): Issue with custom home page on local filesystem
  - [Bug tor-browser#43207](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43207): Backport Mozbug 1886222
- Windows + macOS
  - [Bug tor-browser-build#41252](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41252): Disable updating update_responses in 13.5-legacy branch
- Windows
  - [Bug tor-browser#43206](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/43206): Hide Snowflake UX in legacy Tor Browser on Windows
- Android
  - Updated GeckoView to 115.17.0esr
- Build System
  - All Platforms
    - [Bug tor-browser-build#41278](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41278): Hide legacy 13.5 Tor Browser blog posts after 13.5.7
  - Windows + macOS
    - [Bug tor-browser-build#41269](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/41269): Simplify projects/go/config and friends once maint-13.5 is legacy Windows and macOS only
